#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "seaboy_rom.h"

static void test_rom_not_found(void **state) {
	(void) state; /* unused */
	const char *test_rom = "not-found";
	rom_header_t header;

	assert_false(read_rom_header(test_rom, &header));
}

static void test_invalid_rom(void **state) {
	(void) state; /* unused */
	rom_header_t header;

	/* create a temp file */
	char tmpfile[] = "XXXXXX";
	int fd = mkstemp(tmpfile);

	/* fail fast if the temp file didn't get created */
	assert_true(fd != -1);

	assert_false(read_rom_header(tmpfile, &header));
	assert_true(fclose(fdopen(fd, "r")) == 0);
	unlink(tmpfile);
}

static void test_read_header(void **state) {
	(void) state; /* unused */
	const char *test_rom = "test-header-rom.gb";
	rom_header_t header;

	assert_true(read_rom_header(test_rom, &header));
	assert_memory_equal("TESTING ROM", header.title, sizeof(header.title));
	assert_memory_equal("TEST", header.manufacturer, sizeof(header.manufacturer));
	assert_int_equal(0x80, header.cgb_flags);
	assert_memory_equal("TT", header.licensee_code, sizeof(header.licensee_code));
	assert_int_equal(0x00, header.sgb_flags);
	assert_int_equal(0x00, header.type);
	assert_int_equal(0x00, header.rom_size);
	assert_int_equal(0x00, header.ram_size);
	assert_int_equal(0x01, header.japan_only);
	assert_int_equal(0xff, header.licensee_code_old);
	assert_int_equal(0x00, header.rom_version);
	assert_int_equal(0x89, header.header_checksum);
	assert_int_equal(0x1234, header.checksum);
}

int main(void) {
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_rom_not_found),
		cmocka_unit_test(test_invalid_rom),
		cmocka_unit_test(test_read_header),
	};
	return cmocka_run_group_tests(tests, NULL, NULL);
}
