#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>

#include "seaboy_core.c"

#define OPCODE1(op) \
	{ .main = (op), .low = 0, .high = 0 }
#define OPCODE2(op, val) \
	{ .main = (op), .low = (val), .high = 0 }
#define OPCODE3(op, word) \
	{ .main = (op), .low = (WORD_LOBYTE(word)), .high = (WORD_HIBYTE(word)) }

static void test_illegal(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0xe3);
	assert_ptr_equal(illegal_instr, decode(op));
}

static void test_nop(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x00);
	assert_ptr_equal(NOP, decode(op));
}

static void test_ld_SP_to_mem(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE3(0x08, 0xfaaf);
	assert_ptr_equal(LD_mem_SP, decode(op));
}

static void test_ld_imm16(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE3(0x11, 0xfaaf);
	assert_ptr_equal(LD_reg16_imm16, decode(op));
}

static void test_add_reg_to_HL(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x09);
	assert_ptr_equal(ADD_HL_reg16, decode(op));
}

static void test_ld_A_to_mem(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x02);
	assert_ptr_equal(LD_memreg16_A, decode(op));
}

static void test_ld_mem_to_A(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x0a);
	assert_ptr_equal(LD_A_memreg16, decode(op));
}

static void test_inc_reg16(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x13);
	assert_ptr_equal(INC_reg16, decode(op));
}

static void test_dec_reg16(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x1b);
	assert_ptr_equal(DEC_reg16, decode(op));
}

static void test_inc_reg8(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x34);
	assert_ptr_equal(INC_reg8, decode(op));
}

static void test_dec_reg8(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x3d);
	assert_ptr_equal(DEC_reg8, decode(op));
}

static void test_ld_imm8(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0x16, 0x12);
	assert_ptr_equal(LD_reg8_imm8, decode(op));
}

static void test_rlca(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x07);
	assert_ptr_equal(RLC_A, decode(op));
}

static void test_rrca(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x0f);
	assert_ptr_equal(RRC_A, decode(op));
}

static void test_rla(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x17);
	assert_ptr_equal(RL_A, decode(op));
}

static void test_rra(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x1f);
	assert_ptr_equal(RR_A, decode(op));
}

static void test_stop(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x10);
	assert_ptr_equal(STOP, decode(op));
}

static void test_jr_nocond(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0x18, 0xfb);
	assert_ptr_equal(JR_imm8, decode(op));
}

static void test_jr_cond(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0x20, 0xfb);
	assert_ptr_equal(JR_cond_imm8, decode(op));
}

static void test_ldi_from_A(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x22);
	assert_ptr_equal(LDI_memHL_A, decode(op));
}

static void test_ldi_to_A(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x2a);
	assert_ptr_equal(LDI_A_memHL, decode(op));
}

static void test_ldd_from_A(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x32);
	assert_ptr_equal(LDD_memHL_A, decode(op));
}

static void test_ldd_to_A(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x3a);
	assert_ptr_equal(LDD_A_memHL, decode(op));
}

static void test_daa(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x27);
	assert_ptr_equal(DAA, decode(op));
}

static void test_cpl(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x2f);
	assert_ptr_equal(CPL, decode(op));
}

static void test_scf(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x37);
	assert_ptr_equal(SCF, decode(op));
}

static void test_ccf(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x3f);
	assert_ptr_equal(CCF, decode(op));
}

static void test_ld_reg8_to_reg8(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x47);
	assert_ptr_equal(LD_reg8_reg8, decode(op));
}

static void test_halt(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x76);
	assert_ptr_equal(HALT, decode(op));
}

static void test_alu(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0x81);
	// ADD
	assert_ptr_equal(ALU_A_reg8, decode(op));
	// ADC
	op.main = 0x89;
	assert_ptr_equal(ALU_A_reg8, decode(op));
	// SUB
	op.main = 0x91;
	assert_ptr_equal(ALU_A_reg8, decode(op));
	// SUBC
	op.main = 0x99;
	assert_ptr_equal(ALU_A_reg8, decode(op));
	// AND
	op.main = 0xa1;
	assert_ptr_equal(ALU_A_reg8, decode(op));
	// XOR
	op.main = 0xa9;
	assert_ptr_equal(ALU_A_reg8, decode(op));
	// OR
	op.main = 0xb1;
	assert_ptr_equal(ALU_A_reg8, decode(op));
	// CP
	op.main = 0xb9;
	assert_ptr_equal(ALU_A_reg8, decode(op));
}

static void test_alu_imm8(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xc6, 0x12);
	// ADD
	assert_ptr_equal(ALU_A_imm8, decode(op));
	// ADC
	op.main = 0xce;
	assert_ptr_equal(ALU_A_imm8, decode(op));
	// SUB
	op.main = 0xd6;
	assert_ptr_equal(ALU_A_imm8, decode(op));
	// SUBC
	op.main = 0xde;
	assert_ptr_equal(ALU_A_imm8, decode(op));
	// AND
	op.main = 0xe6;
	assert_ptr_equal(ALU_A_imm8, decode(op));
	// XOR
	op.main = 0xee;
	assert_ptr_equal(ALU_A_imm8, decode(op));
	// OR
	op.main = 0xf6;
	assert_ptr_equal(ALU_A_imm8, decode(op));
	// CP
	op.main = 0xfe;
	assert_ptr_equal(ALU_A_imm8, decode(op));
}

static void test_pop(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0xf1);
	assert_ptr_equal(POP_altreg16, decode(op));
}

static void test_push(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0xf5);
	assert_ptr_equal(PUSH_altreg16, decode(op));
}

static void test_rst(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0xef);
	assert_ptr_equal(RST_num, decode(op));
}

static void test_ret_cond(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0xd0);
	assert_ptr_equal(RET_cond, decode(op));
}

static void test_ret(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0xc9);
	assert_ptr_equal(RET, decode(op));
}

static void test_reti(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0xd9);
	assert_ptr_equal(RETI, decode(op));
}

static void test_jp_imm16_cond(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE3(0xca, 0xfaaf);
	assert_ptr_equal(JP_cond_imm16, decode(op));
}

static void test_jp_imm16(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE3(0xc3, 0xfaaf);
	assert_ptr_equal(JP_imm16, decode(op));
}

static void test_call_imm16_cond(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE3(0xdc, 0xfaaf);
	assert_ptr_equal(CALL_cond_imm16, decode(op));
}

static void test_call_imm16(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE3(0xcd, 0xfaaf);
	assert_ptr_equal(CALL_imm16, decode(op));
}

static void test_add_imm8_to_SP(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xe8, 0x12);
	assert_ptr_equal(ADD_SP_imm8, decode(op));
}

static void test_ld_from_SP_to_HL(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xf8, 0x12);
	assert_ptr_equal(LD_HL_SP_off8, decode(op));
}

static void test_ld_from_imm8_to_A(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xe0, 0x12);
	assert_ptr_equal(LD_mem8_A, decode(op));
}

static void test_ld_from_A_to_imm8(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xf0, 0x12);
	assert_ptr_equal(LD_A_mem8, decode(op));
}

static void test_ld_A_to_indir_C(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0xe2);
	assert_ptr_equal(LD_memC_A, decode(op));
}

static void test_ld_indir_C_to_A(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0xf2);
	assert_ptr_equal(LD_A_memC, decode(op));
}

static void test_ld_A_to_imm_mem(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE3(0xea, 0xfaaf);
	assert_ptr_equal(LD_mem16_A, decode(op));
}

static void test_ld_imm_mem_to_A(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE3(0xfa, 0xfaaf);
	assert_ptr_equal(LD_A_mem16, decode(op));
}

static void test_jp_HL(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0xe9);
	assert_ptr_equal(JP_HL, decode(op));
}

static void test_ld_HL_into_SP(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0xf9);
	assert_ptr_equal(LD_SP_HL, decode(op));
}

static void test_di(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0xf3);
	assert_ptr_equal(DI, decode(op));
}

static void test_ei(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE1(0xfb);
	assert_ptr_equal(EI, decode(op));
}

static void test_rlc(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xcb, 0x02);
	assert_ptr_equal(RLC, decode(op));
}

static void test_rrc(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xcb, 0x0a);
	assert_ptr_equal(RRC, decode(op));
}

static void test_rl(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xcb, 0x12);
	assert_ptr_equal(RL, decode(op));
}

static void test_rr(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xcb, 0x1a);
	assert_ptr_equal(RR, decode(op));
}

static void test_sla(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xcb, 0x22);
	assert_ptr_equal(SLA, decode(op));
}

static void test_sra(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xcb, 0x2a);
	assert_ptr_equal(SRA, decode(op));
}

static void test_swap(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xcb, 0x35);
	assert_ptr_equal(SWAP, decode(op));
}

static void test_srl(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xcb, 0x3d);
	assert_ptr_equal(SRL, decode(op));
}

static void test_bit(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xcb, 0x5f);
	assert_ptr_equal(BIT, decode(op));
}

static void test_res(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xcb, 0xa0);
	assert_ptr_equal(RES, decode(op));
}

static void test_set(void **state) {
	(void) state; /* unused */
	opcode_t op = OPCODE2(0xcb, 0xcc);
	assert_ptr_equal(SET, decode(op));
}

int main(void) {
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_illegal),
		cmocka_unit_test(test_nop),
		cmocka_unit_test(test_ld_SP_to_mem),
		cmocka_unit_test(test_ld_imm16),
		cmocka_unit_test(test_add_reg_to_HL),
		cmocka_unit_test(test_ld_A_to_mem),
		cmocka_unit_test(test_ld_mem_to_A),
		cmocka_unit_test(test_inc_reg16),
		cmocka_unit_test(test_dec_reg16),
		cmocka_unit_test(test_inc_reg8),
		cmocka_unit_test(test_dec_reg8),
		cmocka_unit_test(test_ld_imm8),
		cmocka_unit_test(test_rlca),
		cmocka_unit_test(test_rrca),
		cmocka_unit_test(test_rla),
		cmocka_unit_test(test_rra),
		cmocka_unit_test(test_stop),
		cmocka_unit_test(test_jr_nocond),
		cmocka_unit_test(test_jr_cond),
		cmocka_unit_test(test_ldi_from_A),
		cmocka_unit_test(test_ldi_to_A),
		cmocka_unit_test(test_ldd_from_A),
		cmocka_unit_test(test_ldd_to_A),
		cmocka_unit_test(test_daa),
		cmocka_unit_test(test_cpl),
		cmocka_unit_test(test_scf),
		cmocka_unit_test(test_ccf),
		cmocka_unit_test(test_ld_reg8_to_reg8),
		cmocka_unit_test(test_halt),
		cmocka_unit_test(test_alu),
		cmocka_unit_test(test_alu_imm8),
		cmocka_unit_test(test_pop),
		cmocka_unit_test(test_push),
		cmocka_unit_test(test_rst),
		cmocka_unit_test(test_ret_cond),
		cmocka_unit_test(test_ret),
		cmocka_unit_test(test_reti),
		cmocka_unit_test(test_jp_imm16_cond),
		cmocka_unit_test(test_jp_imm16),
		cmocka_unit_test(test_call_imm16_cond),
		cmocka_unit_test(test_call_imm16),
		cmocka_unit_test(test_add_imm8_to_SP),
		cmocka_unit_test(test_ld_from_SP_to_HL),
		cmocka_unit_test(test_ld_from_imm8_to_A),
		cmocka_unit_test(test_ld_from_A_to_imm8),
		cmocka_unit_test(test_ld_A_to_indir_C),
		cmocka_unit_test(test_ld_indir_C_to_A),
		cmocka_unit_test(test_ld_A_to_imm_mem),
		cmocka_unit_test(test_ld_imm_mem_to_A),
		cmocka_unit_test(test_jp_HL),
		cmocka_unit_test(test_ld_HL_into_SP),
		cmocka_unit_test(test_di),
		cmocka_unit_test(test_ei),
		cmocka_unit_test(test_rlc),
		cmocka_unit_test(test_rrc),
		cmocka_unit_test(test_rl),
		cmocka_unit_test(test_rr),
		cmocka_unit_test(test_sra),
		cmocka_unit_test(test_sla),
		cmocka_unit_test(test_swap),
		cmocka_unit_test(test_srl),
		cmocka_unit_test(test_bit),
		cmocka_unit_test(test_res),
		cmocka_unit_test(test_set),
	};
	return cmocka_run_group_tests(tests, NULL, NULL);
}
