#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>

#include "seaboy_mmu.h"
#include "seaboy_timer.h"

static int setup(void **state) {
	(void) state; /* unused */
	init_timer(0UL);
	return 0;
}

static void test_timer_overflow(void **state) {
	(void) state; /* unused */
	mem_write8(IF_ADDRESS, 0x00);
	mem_write8(TAC_ADDRESS, 0x04);
	mem_write8(TIMA_ADDRESS, 0x11);

	timer(1024);
	assert_false(mem_read8(IF_ADDRESS) & 0x04);
	assert_int_equal(0x12, mem_read8(TIMA_ADDRESS));
}

static void test_clock_selector0(void **state) {
	(void) state; /* unused */
	mem_write8(TAC_ADDRESS, 0x04);
	mem_write8(TIMA_ADDRESS, 0xff);
	mem_write8(TMA_ADDRESS, 0x12);

	timer(1024);
	assert_true(mem_read8(IF_ADDRESS) & 0x04);
	assert_int_equal(0x12, mem_read8(TIMA_ADDRESS));
}

static void test_clock_selector1(void **state) {
	(void) state; /* unused */
	mem_write8(TAC_ADDRESS, 0x05);
	mem_write8(TIMA_ADDRESS, 0xff);
	mem_write8(TMA_ADDRESS, 0x12);

	timer(16);
	assert_true(mem_read8(IF_ADDRESS) & 0x04);
	assert_int_equal(0x12, mem_read8(TIMA_ADDRESS));
}

static void test_clock_selector2(void **state) {
	(void) state; /* unused */
	mem_write8(TAC_ADDRESS, 0x06);
	mem_write8(TIMA_ADDRESS, 0xff);
	mem_write8(TMA_ADDRESS, 0x12);

	timer(64);
	assert_true(mem_read8(IF_ADDRESS) & 0x04);
	assert_int_equal(0x12, mem_read8(TIMA_ADDRESS));
}

static void test_clock_selector3(void **state) {
	(void) state; /* unused */
	mem_write8(TAC_ADDRESS, 0x07);
	mem_write8(TIMA_ADDRESS, 0xff);
	mem_write8(TMA_ADDRESS, 0x12);

	timer(256);
	assert_true(mem_read8(IF_ADDRESS) & 0x04);
	assert_int_equal(0x12, mem_read8(TIMA_ADDRESS));
}

int main(void) {
	const struct CMUnitTest tests[] = {
		cmocka_unit_test_setup(test_timer_overflow, setup),
		cmocka_unit_test_setup(test_clock_selector0, setup),
		cmocka_unit_test_setup(test_clock_selector1, setup),
		cmocka_unit_test_setup(test_clock_selector2, setup),
		cmocka_unit_test_setup(test_clock_selector3, setup),
	};
	return cmocka_run_group_tests(tests, NULL, NULL);
}
