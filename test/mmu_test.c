#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>

#include "seaboy_mmu.h"
#include "utils.h"

static void test_address_in_rom0(void **state) {
	(void) state; /* unused */
	const gb_memory_t *mem = get_memory();
	const uint16_t addr = 0x0100;
	uint8_t *block;

	const uint16_t offset = resolve_address(addr, &block);

	assert_ptr_equal(mem->romBank0, block);
	assert_int_equal(0x0100, offset);
}

static void test_address_in_switchable_rom(void **state) {
	(void) state; /* unused */
	const gb_memory_t *mem = get_memory();
	const uint16_t addr = 0x4444;
	uint8_t *block;

	const uint16_t offset = resolve_address(addr, &block);

	assert_ptr_equal(mem->romSwitchBank, block);
	assert_int_equal(0x0444, offset);
}

static void test_address_in_ram_video(void **state) {
	(void) state; /* unused */
	const gb_memory_t *mem = get_memory();
	const uint16_t addr = 0x8000;
	uint8_t *block;

	const uint16_t offset = resolve_address(addr, &block);

	assert_ptr_equal(mem->ramVideo, block);
	assert_int_equal(0x0000, offset);
}

static void test_address_in_switchable_ram(void **state) {
	(void) state; /* unused */
	const gb_memory_t *mem = get_memory();
	const uint16_t addr = 0xa123;
	uint8_t *block;

	const uint16_t offset = resolve_address(addr, &block);

	assert_ptr_equal(mem->ramSwitchBank, block);
	assert_int_equal(0x0123, offset);
}

static void test_address_in_ram(void **state) {
	(void) state; /* unused */
	const gb_memory_t *mem = get_memory();
	const uint16_t addr = 0xd000;
	uint8_t *block;

	const uint16_t offset = resolve_address(addr, &block);

	assert_ptr_equal(mem->ram, block);
	assert_int_equal(0x1000, offset);
}

static void test_address_in_ramOAM(void **state) {
	(void) state; /* unused */
	const gb_memory_t *mem = get_memory();
	const uint16_t addr = 0xfe33;
	uint8_t *block;

	const uint16_t offset = resolve_address(addr, &block);

	assert_ptr_equal(mem->ramOAM, block);
	assert_int_equal(0x0033, offset);
}

static void test_address_in_ioports(void **state) {
	(void) state; /* unused */
	const gb_memory_t *mem = get_memory();
	const uint16_t addr = 0xff10;
	uint8_t *block;

	const uint16_t offset = resolve_address(addr, &block);

	assert_ptr_equal(mem->ioports, block);
	assert_int_equal(0x0010, offset);
}

static void test_address_in_stack(void **state) {
	(void) state; /* unused */
	const gb_memory_t *mem = get_memory();
	const uint16_t addr = 0xfffe;
	uint8_t *block;

	const uint16_t offset = resolve_address(addr, &block);

	assert_ptr_equal(mem->stack, block);
	assert_int_equal(0x007e, offset);
}

static void test_address_in_unusable(void **state) {
	(void) state; /* unused */
	const uint16_t addr = 0xfea1;
	uint8_t *block = NULL;
	uint16_t offset;

	// should always reads as 0
	offset = resolve_address(addr, &block);
	assert_int_equal(0, offset);
	assert_int_equal(0, *block);

	// writes should not have any effect
	*block = 1;
	offset = resolve_address(addr, &block);
	assert_int_equal(0, offset);
	assert_int_equal(0, *block);
}

static void test_address_in_ram_echo(void **state) {
	(void) state; /* unused */
	const gb_memory_t *mem = get_memory();
	uint8_t *block;

	const uint16_t offset = resolve_address(0xe001, &block);
	assert_ptr_equal(mem->ram, block);
	assert_int_equal(0x0001, offset);
}

static void test_read_and_write8(void **state) {
	(void) state; /* unused */
	const uint8_t data = 0x12;
	const uint16_t address = 0xd123;

	assert_true(mem_write8(address, data));
	assert_int_equal(data, mem_read8(address));
}

static void test_read_and_write16(void **state) {
	(void) state; /* unused */
	const uint8_t low = 0xaf;
	const uint8_t high = 0xfa;
	const uint16_t data = TO_WORD(high, low);
	const uint16_t address = 0xd123;

	assert_true(mem_write16(address, data));
	assert_int_equal(data, mem_read16(address));
	assert_int_equal(low, mem_read8(address));
	assert_int_equal(high, mem_read8(address + 1));
}

static void test_dma_transfer(void **state) {
	(void) state; /* unused */
	const uint16_t src_addr = 0x8100;
	const uint16_t dst_addr = DMA_BASE_ADDRESS;

	for (uint16_t i = 0; i < DMA_TRANSFER_SIZE; i++) {
		mem_write8(src_addr + i, i);
		mem_write8(dst_addr + i, 0);
	}

	assert_true(mem_write8(DMA_ADDRESS, src_addr >> 8));

	for (uint16_t i = 0; i < DMA_TRANSFER_SIZE; i++) {
		assert_int_equal(mem_read8(src_addr + i), mem_read8(dst_addr + i));
	}
}

int main(void) {
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_address_in_rom0),
		cmocka_unit_test(test_address_in_switchable_rom),
		cmocka_unit_test(test_address_in_ram_video),
		cmocka_unit_test(test_address_in_switchable_ram),
		cmocka_unit_test(test_address_in_ram),
		cmocka_unit_test(test_address_in_ramOAM),
		cmocka_unit_test(test_address_in_ioports),
		cmocka_unit_test(test_address_in_stack),
		cmocka_unit_test(test_address_in_unusable),
		cmocka_unit_test(test_address_in_ram_echo),
		cmocka_unit_test(test_read_and_write8),
		cmocka_unit_test(test_read_and_write16),
		cmocka_unit_test(test_dma_transfer),
	};
	return cmocka_run_group_tests(tests, NULL, NULL);
}
