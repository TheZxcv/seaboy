#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>

#include "seaboy_core.c"

static void test_carry_flag_8(void **state) {
	(void) state; /* unused */

	CLEARF(FLAG_C, regs);
	compute_carry(BYTE, 0xff, 0x01, 0x100);
	assert_true(GETF(FLAG_C, regs));

	CLEARF(FLAG_C, regs);
	compute_carry(BYTE, 0xfe, 0x01, 0xff);
	assert_false(GETF(FLAG_C, regs));
}

static void test_carry_flag_16(void **state) {
	(void) state; /* unused */

	CLEARF(FLAG_C, regs);
	compute_carry(WORD, 0xf111, 0x1000, 0x10111);
	assert_true(GETF(FLAG_C, regs));

	CLEARF(FLAG_C, regs);
	compute_carry(WORD, 0x1030, 0x0204, 0x1234);
	assert_false(GETF(FLAG_C, regs));
}

static void test_halfcarry_flag_8(void **state) {
	(void) state; /* unused */

	CLEARF(FLAG_H, regs);
	half_carry(BYTE, 0x1f, 0x01, 0x20);
	assert_true(GETF(FLAG_H, regs));

	CLEARF(FLAG_H, regs);
	half_carry(BYTE, 0x1e, 0x01, 0x1f);
	assert_false(GETF(FLAG_H, regs));
}

static void test_halfcarry_flag_16(void **state) {
	(void) state; /* unused */

	CLEARF(FLAG_H, regs);
	half_carry(WORD, 0x1fff, 0x0001, 0x2000);
	assert_true(GETF(FLAG_H, regs));

	CLEARF(FLAG_H, regs);
	half_carry(WORD, 0x1234, 0x4321, 0x5555);
	assert_false(GETF(FLAG_H, regs));
}

static void test_borrow_flag(void **state) {
	(void) state; /* unused */

	CLEARF(FLAG_C, regs);
	compute_borrow(0x00, 0x01);
	assert_true(GETF(FLAG_C, regs));

	CLEARF(FLAG_C, regs);
	compute_borrow(0x01, 0x01);
	assert_false(GETF(FLAG_C, regs));
}

static void test_halfborrow_flag_8(void **state) {
	(void) state; /* unused */

	CLEARF(FLAG_H, regs);
	half_borrow(BYTE, 0x10, 0x01);
	assert_true(GETF(FLAG_H, regs));

	CLEARF(FLAG_H, regs);
	half_borrow(BYTE, 0xff, 0xff);
	assert_false(GETF(FLAG_H, regs));
}

static void test_halfborrow_flag_16(void **state) {
	(void) state; /* unused */

	CLEARF(FLAG_H, regs);
	half_borrow(WORD, 0x1000, 0x0001);
	assert_true(GETF(FLAG_H, regs));

	CLEARF(FLAG_H, regs);
	half_borrow(WORD, 0xffff, 0xffff);
	assert_false(GETF(FLAG_H, regs));
}

static void test_check_condition(void **state) {
	(void) state; /* unused */

	CLEARF(FLAG_Z, regs);
	assert_true(check_condition(COND_NZ));
	assert_false(check_condition(COND_Z));

	SETF(FLAG_Z, regs);
	assert_true(check_condition(COND_Z));
	assert_false(check_condition(COND_NZ));

	CLEARF(FLAG_C, regs);
	assert_true(check_condition(COND_NC));
	assert_false(check_condition(COND_C));

	SETF(FLAG_C, regs);
	assert_true(check_condition(COND_C));
	assert_false(check_condition(COND_NC));
}

static void test_operand_decode(void **state) {
	(void) state; /* unused */

	assert_int_equal(REG_B, decode_operand8(0x0));
	assert_int_equal(REG_C, decode_operand8(0x1));
	assert_int_equal(REG_D, decode_operand8(0x2));
	assert_int_equal(REG_E, decode_operand8(0x3));
	assert_int_equal(REG_H, decode_operand8(0x4));
	assert_int_equal(REG_L, decode_operand8(0x5));
	assert_int_equal(MEM_HL, decode_operand8(0x6));
	assert_int_equal(REG_A, decode_operand8(0x7));

	assert_int_equal(REG_BC, decode_operand16(0x0));
	assert_int_equal(REG_DE, decode_operand16(0x1));
	assert_int_equal(REG_HL, decode_operand16(0x2));
	assert_int_equal(REG_SP, decode_operand16(0x3));

	assert_int_equal(decode_operand16(0x0), decode_operand16_alt(0x0));
	assert_int_equal(decode_operand16(0x1), decode_operand16_alt(0x1));
	assert_int_equal(decode_operand16(0x2), decode_operand16_alt(0x2));
	assert_int_equal(REG_AF, decode_operand16_alt(0x3));
}

#define TEST_OPERAND8(reg, operand)                    \
	do {                                               \
		(reg) = 0x12;                                  \
		assert_int_equal(0x12, get_value8((operand))); \
		set_value8((operand), 0x34);                   \
		assert_int_equal(0x34, (reg));                 \
		assert_int_equal(0x34, get_value8((operand))); \
	} while (0)

static void test_operand8(void **state) {
	(void) state; /* unused */

	TEST_OPERAND8(regs.A, REG_A);
	TEST_OPERAND8(regs.B, REG_B);
	TEST_OPERAND8(regs.C, REG_C);
	TEST_OPERAND8(regs.D, REG_D);
	TEST_OPERAND8(regs.E, REG_E);
	TEST_OPERAND8(regs.H, REG_H);
	TEST_OPERAND8(regs.L, REG_L);

	uint16_t addr = 0x8001;
	SET_PAIR(regs.H, regs.L, addr);
	mem_write8(addr, 0x12);
	assert_int_equal(0x12, get_value8(MEM_HL));
	set_value8(MEM_HL, 0x34);
	assert_int_equal(0x34, get_value8(MEM_HL));
	assert_int_equal(0x34, mem_read8(addr));
}
#undef TEST_OPERAND8

#define TEST_OPERAND16(regH, regL, operand)                \
	do {                                                   \
		SET_PAIR((regH), (regL), 0x1234);                  \
		assert_int_equal(0x1234, get_value16((operand)));  \
		set_value16((operand), 0x5678);                    \
		assert_int_equal(0x5678, TO_WORD((regH), (regL))); \
		assert_int_equal(0x5678, get_value16((operand)));  \
	} while (0)

static void test_operand16(void **state) {
	(void) state; /* unused */
	TEST_OPERAND16(regs.B, regs.C, REG_BC);
	TEST_OPERAND16(regs.D, regs.E, REG_DE);
	TEST_OPERAND16(regs.H, regs.L, REG_HL);

	SET_PAIR(regs.A, regs.F, 0x1234);
	assert_int_equal(0x1230, get_value16(REG_AF));
	set_value16(REG_AF, 0x5678);
	assert_int_equal(0x5670, TO_WORD(regs.A, regs.F));
	assert_int_equal(0x5670, get_value16(REG_AF));

	regs.SP = 0x1234;
	assert_int_equal(0x1234, get_value16(REG_SP));
	set_value16(REG_SP, 0x5678);
	assert_int_equal(0x5678, regs.SP);
	assert_int_equal(0x5678, get_value16(REG_SP));
}
#undef TEST_OPERAND16

static void test_add(void **state) {
	(void) state; /* unused */
	regs.A = 0xff;
	alu(ADD, 0x01);

	assert_int_equal(0x00, regs.A);
	assert_true(GETF(FLAG_Z, regs));
	assert_true(GETF(FLAG_C, regs));
	assert_true(GETF(FLAG_H, regs));
	assert_false(GETF(FLAG_N, regs));
}

static void test_adc(void **state) {
	(void) state; /* unused */
	regs.A = 0xff;
	SETF(FLAG_C, regs);
	alu(ADC, 0x01);

	assert_int_equal(0x01, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_true(GETF(FLAG_C, regs));
	assert_true(GETF(FLAG_H, regs));
	assert_false(GETF(FLAG_N, regs));

	regs.A = 0xff;
	SETF(FLAG_C, regs);
	alu(ADC, 0xff);

	assert_int_equal(0xff, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_true(GETF(FLAG_C, regs));
	assert_true(GETF(FLAG_H, regs));
	assert_false(GETF(FLAG_N, regs));
}

static void test_sub(void **state) {
	(void) state; /* unused */

	regs.A = 0x00;
	alu(SUB, 0x01);
	assert_int_equal(0xff, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_true(GETF(FLAG_C, regs));
	assert_true(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));

	regs.A = 0x00;
	alu(SUB, 0xff);
	assert_int_equal(0x01, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_true(GETF(FLAG_C, regs));
	assert_true(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));

	regs.A = 0xff;
	alu(SUB, 0x01);
	assert_int_equal(0xfe, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_false(GETF(FLAG_C, regs));
	assert_false(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));

	regs.A = 0x10;
	alu(SUB, 0x01);
	assert_int_equal(0x0f, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_false(GETF(FLAG_C, regs));
	assert_true(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));

	regs.A = 0x10;
	alu(SUB, 0x20);
	assert_int_equal(0xf0, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_true(GETF(FLAG_C, regs));
	assert_false(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));

	regs.A = 0xff;
	alu(SUB, 0xff);
	assert_int_equal(0x00, regs.A);
	assert_true(GETF(FLAG_Z, regs));
	assert_false(GETF(FLAG_C, regs));
	assert_false(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));
}

static void test_sbc(void **state) {
	(void) state; /* unused */

	regs.A = 0x00;
	SETF(FLAG_C, regs);
	alu(SBC, 0x01);
	assert_int_equal(0xfe, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_true(GETF(FLAG_C, regs));
	assert_true(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));

	regs.A = 0xff;
	SETF(FLAG_C, regs);
	alu(SBC, 0xff);
	assert_int_equal(0xff, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_true(GETF(FLAG_C, regs));
	assert_true(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));

	regs.A = 0x02;
	SETF(FLAG_C, regs);
	alu(SBC, 0x01);
	assert_int_equal(0x00, regs.A);
	assert_true(GETF(FLAG_Z, regs));
	assert_false(GETF(FLAG_C, regs));
	assert_false(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));
}

static void test_and(void **state) {
	(void) state; /* unused */

	regs.A = 0xff;
	alu(AND, 0xff);
	assert_int_equal(0xff, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_false(GETF(FLAG_C, regs));
	assert_true(GETF(FLAG_H, regs));
	assert_false(GETF(FLAG_N, regs));

	regs.A = 0x00;
	alu(AND, 0x00);
	assert_int_equal(0x00, regs.A);
	assert_true(GETF(FLAG_Z, regs));
	assert_false(GETF(FLAG_C, regs));
	assert_true(GETF(FLAG_H, regs));
	assert_false(GETF(FLAG_N, regs));

	regs.A = 0xff;
	SETF(FLAG_C, regs);
	alu(AND, 0x80);
	assert_int_equal(0x80, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_false(GETF(FLAG_C, regs));
	assert_true(GETF(FLAG_H, regs));
	assert_false(GETF(FLAG_N, regs));
}

static void test_xor(void **state) {
	(void) state; /* unused */

	regs.A = 0x00;
	alu(XOR, 0xff);
	assert_int_equal(0xff, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_false(GETF(FLAG_C, regs));
	assert_false(GETF(FLAG_H, regs));
	assert_false(GETF(FLAG_N, regs));

	regs.A = 0xAB;
	alu(XOR, 0xAB);
	assert_int_equal(0x00, regs.A);
	assert_true(GETF(FLAG_Z, regs));
	assert_false(GETF(FLAG_C, regs));
	assert_false(GETF(FLAG_H, regs));
	assert_false(GETF(FLAG_N, regs));
}

static void test_or(void **state) {
	(void) state; /* unused */

	regs.A = 0x00;
	alu(OR, 0x00);
	assert_int_equal(0x00, regs.A);
	assert_true(GETF(FLAG_Z, regs));
	assert_false(GETF(FLAG_C, regs));
	assert_false(GETF(FLAG_H, regs));
	assert_false(GETF(FLAG_N, regs));

	regs.A = 0xcc;
	alu(OR, 0x33);
	assert_int_equal(0xff, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_false(GETF(FLAG_C, regs));
	assert_false(GETF(FLAG_H, regs));
	assert_false(GETF(FLAG_N, regs));
}

static void test_cp(void **state) {
	(void) state; /* unused */

	regs.A = 0x00;
	alu(CP, 0x01);
	assert_int_equal(0x00, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_true(GETF(FLAG_C, regs));
	assert_true(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));

	regs.A = 0x00;
	alu(CP, 0xff);
	assert_int_equal(0x00, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_true(GETF(FLAG_C, regs));
	assert_true(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));

	regs.A = 0xff;
	alu(CP, 0x01);
	assert_int_equal(0xff, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_false(GETF(FLAG_C, regs));
	assert_false(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));

	regs.A = 0x10;
	alu(CP, 0x01);
	assert_int_equal(0x10, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_false(GETF(FLAG_C, regs));
	assert_true(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));

	regs.A = 0x10;
	alu(CP, 0x20);
	assert_int_equal(0x10, regs.A);
	assert_false(GETF(FLAG_Z, regs));
	assert_true(GETF(FLAG_C, regs));
	assert_false(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));

	regs.A = 0xff;
	alu(CP, 0xff);
	assert_int_equal(0xff, regs.A);
	assert_true(GETF(FLAG_Z, regs));
	assert_false(GETF(FLAG_C, regs));
	assert_false(GETF(FLAG_H, regs));
	assert_true(GETF(FLAG_N, regs));
}

int main(void) {
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_carry_flag_8),
		cmocka_unit_test(test_carry_flag_16),
		cmocka_unit_test(test_halfcarry_flag_8),
		cmocka_unit_test(test_halfcarry_flag_16),
		cmocka_unit_test(test_borrow_flag),
		cmocka_unit_test(test_halfborrow_flag_8),
		cmocka_unit_test(test_halfborrow_flag_16),
		cmocka_unit_test(test_check_condition),
		cmocka_unit_test(test_operand_decode),
		cmocka_unit_test(test_operand8),
		cmocka_unit_test(test_operand16),
		cmocka_unit_test(test_add),
		cmocka_unit_test(test_adc),
		cmocka_unit_test(test_sub),
		cmocka_unit_test(test_sbc),
		cmocka_unit_test(test_and),
		cmocka_unit_test(test_xor),
		cmocka_unit_test(test_or),
		cmocka_unit_test(test_cp),
	};
	return cmocka_run_group_tests(tests, NULL, NULL);
}
