#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "seaboy.h"
#include "seaboy_mmu.h"

static bool matched;
bool run_test_rom(const char *filename) {
	if (!load_rom(filename))
		return false;
	seaboy_init();

	matched = false;
	seaboy_run();
	return matched;
}

static char *pattern = "Passed";
static size_t next_char = 0;
void matcher(uint8_t data) {
	// tries to match the word "Passed"
	if (!matched) {
		if (data == pattern[next_char])
			next_char++;
		else
			next_char = 0;
		if (next_char >= strlen(pattern))
			matched = true;
	}
}

int main(void) {
	set_serial_callback(matcher);
	/* clang-format off */
	const char *roms[] = {
		"01-special.gb",
		"02-interrupts.gb",
		"03-op sp,hl.gb",
		"04-op r,imm.gb",
		"05-op rp.gb",
		"06-ld r,r.gb",
		"07-jr,jp,call,ret,rst.gb",
		"08-misc instrs.gb",
		"09-op r,r.gb",
		"10-bit ops.gb",
		"11-op a,(hl).gb",
        "instr_timing.gb",
	};
	/* clang-format on */
	const size_t len = sizeof(roms) / sizeof(char *);

	for (size_t i = 0; i < len; i++) {
		if (!run_test_rom(roms[i])) {
			printf("[FAIL] %s\n", roms[i]);
			return EXIT_FAILURE;
		} else {
			printf("[ OK ] %s\n", roms[i]);
		}
	}
	return EXIT_SUCCESS;
}
