#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>

#include <string.h>

#include "seaboy_debug.h"

static void test_invalid(void **state) {
	(void) state; /* unused */
	char buffer[128];
	const uint8_t opcodes[] = { 0xe3 };
	assert_false(disass(opcodes, buffer, 128));
}

#define BUFLEN 6
static void test_buffer_too_short(void **state) {
	(void) state; /* unused */
	char buffer[BUFLEN];
	const uint8_t opcodes[] = { 0x13 }; // INC DE
	(void) disass(opcodes, buffer, BUFLEN);

	assert_int_equal(BUFLEN - 1, strlen(buffer));
	assert_string_equal("INC D", buffer);
}
#undef BUFLEN

static void test_instr(const char *expected, const uint8_t *opcodes) {
	char buffer[128];
	(void) disass(opcodes, buffer, 128);
	assert_string_equal(expected, buffer);
}

static void test_nop(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0 };
	test_instr("NOP", opcodes);
}

static void test_ld_SP_to_mem(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x08, 0xaf, 0xfa };
	test_instr("LD (0xfaaf), SP", opcodes);
}

static void test_ld_imm16(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x11, 0xaf, 0xfa };
	test_instr("LD DE, 0xfaaf", opcodes);
}

static void test_add_reg_to_HL(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x09 };
	test_instr("ADD HL, BC", opcodes);
}

static void test_ld_A_to_mem(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x02 };
	test_instr("LD (BC), A", opcodes);
}

static void test_ld_mem_to_A(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x0a };
	test_instr("LD A, (BC)", opcodes);
}

static void test_inc_reg16(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x13 };
	test_instr("INC DE", opcodes);
}

static void test_dec_reg16(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x1b };
	test_instr("DEC DE", opcodes);
}

static void test_inc_reg8(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x34 };
	test_instr("INC (HL)", opcodes);
}

static void test_dec_reg8(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x3d };
	test_instr("DEC A", opcodes);
}

static void test_ld_imm8(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x16, 0x12 };
	test_instr("LD D, 0x12", opcodes);
}

static void test_rlca(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x07 };
	test_instr("RLC A", opcodes);
}

static void test_rrca(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x0f };
	test_instr("RRC A", opcodes);
}

static void test_rla(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x17 };
	test_instr("RL A", opcodes);
}

static void test_rra(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x1f };
	test_instr("RR A", opcodes);
}

static void test_stop(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x10 };
	test_instr("STOP", opcodes);
}

static void test_jr_nocond(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x18, 0xfb };
	test_instr("JR 0xfb", opcodes);
}

static void test_jr_cond(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x20, 0xfb };
	test_instr("JR NZ, 0xfb", opcodes);
}

static void test_ldi_from_A(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x22 };
	test_instr("LD (HL+), A", opcodes);
}

static void test_ldi_to_A(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x2a };
	test_instr("LD A, (HL+)", opcodes);
}

static void test_ldd_from_A(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x32 };
	test_instr("LD (HL-), A", opcodes);
}

static void test_ldd_to_A(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x3a };
	test_instr("LD A, (HL-)", opcodes);
}

static void test_daa(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x27 };
	test_instr("DAA", opcodes);
}

static void test_cpl(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x2f };
	test_instr("CPL", opcodes);
}

static void test_scf(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x37 };
	test_instr("SCF", opcodes);
}

static void test_ccf(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x3f };
	test_instr("CCF", opcodes);
}

static void test_ld_reg8_to_reg8(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x47 };
	test_instr("LD B, A", opcodes);
}

static void test_halt(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0x76 };
	test_instr("HALT", opcodes);
}

static void test_alu(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[1];

	opcodes[0] = 0x81;
	test_instr("ADD A, C", opcodes);
	opcodes[0] = 0x89;
	test_instr("ADC A, C", opcodes);
	opcodes[0] = 0x91;
	test_instr("SUB A, C", opcodes);
	opcodes[0] = 0x99;
	test_instr("SBC A, C", opcodes);
	opcodes[0] = 0xa1;
	test_instr("AND A, C", opcodes);
	opcodes[0] = 0xa9;
	test_instr("XOR A, C", opcodes);
	opcodes[0] = 0xb1;
	test_instr("OR A, C", opcodes);
	opcodes[0] = 0xb9;
	test_instr("CP A, C", opcodes);
}

static void test_alu_imm8(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[2] = { 0x00, 0x12 };

	opcodes[0] = 0xc6;
	test_instr("ADD A, 0x12", opcodes);
	opcodes[0] = 0xce;
	test_instr("ADC A, 0x12", opcodes);
	opcodes[0] = 0xd6;
	test_instr("SUB A, 0x12", opcodes);
	opcodes[0] = 0xde;
	test_instr("SBC A, 0x12", opcodes);
	opcodes[0] = 0xe6;
	test_instr("AND A, 0x12", opcodes);
	opcodes[0] = 0xee;
	test_instr("XOR A, 0x12", opcodes);
	opcodes[0] = 0xf6;
	test_instr("OR A, 0x12", opcodes);
	opcodes[0] = 0xfe;
	test_instr("CP A, 0x12", opcodes);
}

static void test_pop(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xf1 };
	test_instr("POP AF", opcodes);
}

static void test_push(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xf5 };
	test_instr("PUSH AF", opcodes);
}

static void test_rst(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xef };
	test_instr("RST 0x05", opcodes);
}

static void test_ret_cond(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xd0 };
	test_instr("RET NC", opcodes);
}

static void test_ret(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xc9 };
	test_instr("RET", opcodes);
}

static void test_reti(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xd9 };
	test_instr("RETI", opcodes);
}

static void test_jp_imm16_cond(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xca, 0xaf, 0xfa };
	test_instr("JP Z, 0xfaaf", opcodes);
}

static void test_jp_imm16(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xc3, 0xaf, 0xfa };
	test_instr("JP 0xfaaf", opcodes);
}

static void test_call_imm16_cond(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xdc, 0xaf, 0xfa };
	test_instr("CALL C, 0xfaaf", opcodes);
}

static void test_call_imm16(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xcd, 0xaf, 0xfa };
	test_instr("CALL 0xfaaf", opcodes);
}

static void test_add_imm8_to_SP(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xe8, 0x12 };
	test_instr("ADD SP, 0x12", opcodes);
}

static void test_ld_from_SP_to_HL(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xf8, 0x12 };
	test_instr("LD HL, SP+0x12", opcodes);
}

static void test_ld_from_imm8_to_A(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xe0, 0x12 };
	test_instr("LD (0xff00 + 0x12), A", opcodes);
}

static void test_ld_from_A_to_imm8(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xf0, 0x12 };
	test_instr("LD A, (0xff00 + 0x12)", opcodes);
}

static void test_ld_A_to_indir_C(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xe2 };
	test_instr("LD (C), A", opcodes);
}

static void test_ld_indir_C_to_A(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xf2 };
	test_instr("LD A, (C)", opcodes);
}

static void test_ld_A_to_imm_mem(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xea, 0xaf, 0xfa };
	test_instr("LD (0xfaaf), A", opcodes);
}

static void test_ld_imm_mem_to_A(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xfa, 0xaf, 0xfa };
	test_instr("LD A, (0xfaaf)", opcodes);
}

static void test_jp_HL(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xe9 };
	test_instr("JP HL", opcodes);
}

static void test_ld_HL_into_SP(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xf9 };
	test_instr("LD SP, HL", opcodes);
}

static void test_di(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xf3 };
	test_instr("DI", opcodes);
}

static void test_ei(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xfb };
	test_instr("EI", opcodes);
}

static void test_rlc(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xcb, 0x02 };
	test_instr("RLC D", opcodes);
}

static void test_rrc(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xcb, 0x0a };
	test_instr("RRC D", opcodes);
}

static void test_rl(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xcb, 0x12 };
	test_instr("RL D", opcodes);
}

static void test_rr(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xcb, 0x1a };
	test_instr("RR D", opcodes);
}

static void test_sla(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xcb, 0x22 };
	test_instr("SLA D", opcodes);
}

static void test_sra(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xcb, 0x2a };
	test_instr("SRA D", opcodes);
}

static void test_swap(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xcb, 0x35 };
	test_instr("SWAP L", opcodes);
}

static void test_srl(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xcb, 0x3d };
	test_instr("SRL L", opcodes);
}

static void test_bit(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xcb, 0x5f };
	test_instr("BIT 0x03, A", opcodes);
}

static void test_res(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xcb, 0xa0 };
	test_instr("RES 0x04, B", opcodes);
}

static void test_set(void **state) {
	(void) state; /* unused */
	uint8_t opcodes[] = { 0xcb, 0xcc };
	test_instr("SET 0x01, H", opcodes);
}

int main(void) {
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_invalid),
		cmocka_unit_test(test_buffer_too_short),
		cmocka_unit_test(test_nop),
		cmocka_unit_test(test_ld_SP_to_mem),
		cmocka_unit_test(test_ld_imm16),
		cmocka_unit_test(test_add_reg_to_HL),
		cmocka_unit_test(test_ld_A_to_mem),
		cmocka_unit_test(test_ld_mem_to_A),
		cmocka_unit_test(test_inc_reg16),
		cmocka_unit_test(test_dec_reg16),
		cmocka_unit_test(test_inc_reg8),
		cmocka_unit_test(test_dec_reg8),
		cmocka_unit_test(test_ld_imm8),
		cmocka_unit_test(test_rlca),
		cmocka_unit_test(test_rrca),
		cmocka_unit_test(test_rla),
		cmocka_unit_test(test_rra),
		cmocka_unit_test(test_stop),
		cmocka_unit_test(test_jr_nocond),
		cmocka_unit_test(test_jr_cond),
		cmocka_unit_test(test_ldi_from_A),
		cmocka_unit_test(test_ldi_to_A),
		cmocka_unit_test(test_ldd_from_A),
		cmocka_unit_test(test_ldd_to_A),
		cmocka_unit_test(test_daa),
		cmocka_unit_test(test_cpl),
		cmocka_unit_test(test_scf),
		cmocka_unit_test(test_ccf),
		cmocka_unit_test(test_ld_reg8_to_reg8),
		cmocka_unit_test(test_halt),
		cmocka_unit_test(test_alu),
		cmocka_unit_test(test_alu_imm8),
		cmocka_unit_test(test_pop),
		cmocka_unit_test(test_push),
		cmocka_unit_test(test_rst),
		cmocka_unit_test(test_ret_cond),
		cmocka_unit_test(test_ret),
		cmocka_unit_test(test_reti),
		cmocka_unit_test(test_jp_imm16_cond),
		cmocka_unit_test(test_jp_imm16),
		cmocka_unit_test(test_call_imm16_cond),
		cmocka_unit_test(test_call_imm16),
		cmocka_unit_test(test_add_imm8_to_SP),
		cmocka_unit_test(test_ld_from_SP_to_HL),
		cmocka_unit_test(test_ld_from_imm8_to_A),
		cmocka_unit_test(test_ld_from_A_to_imm8),
		cmocka_unit_test(test_ld_A_to_indir_C),
		cmocka_unit_test(test_ld_indir_C_to_A),
		cmocka_unit_test(test_ld_A_to_imm_mem),
		cmocka_unit_test(test_ld_imm_mem_to_A),
		cmocka_unit_test(test_jp_HL),
		cmocka_unit_test(test_ld_HL_into_SP),
		cmocka_unit_test(test_di),
		cmocka_unit_test(test_ei),
		cmocka_unit_test(test_rlc),
		cmocka_unit_test(test_rrc),
		cmocka_unit_test(test_rl),
		cmocka_unit_test(test_rr),
		cmocka_unit_test(test_sra),
		cmocka_unit_test(test_sla),
		cmocka_unit_test(test_swap),
		cmocka_unit_test(test_srl),
		cmocka_unit_test(test_bit),
		cmocka_unit_test(test_res),
		cmocka_unit_test(test_set),
	};
	return cmocka_run_group_tests(tests, NULL, NULL);
}
