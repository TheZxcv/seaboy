set(PROJECT_TEST_DIR "${CMAKE_CURRENT_SOURCE_DIR}")

add_executable(disassembly_test disass_test.c)
add_test(disassembly_test disassembly_test)
target_link_libraries(disassembly_test seaboy cmocka)

add_executable(rom_read_test rom_read_test.c)
add_test(NAME rom_read_test COMMAND rom_read_test WORKING_DIRECTORY "${PROJECT_TEST_DIR}")
target_link_libraries(rom_read_test seaboy cmocka)

add_executable(mmu_test mmu_test.c)
add_test(mmu_test mmu_test)
target_link_libraries(mmu_test seaboy cmocka)

add_executable(decode_test decode_test.c)
target_include_directories(decode_test PRIVATE "${CMAKE_SOURCE_DIR}/src")
add_test(decode_test decode_test)
target_link_libraries(decode_test seaboy cmocka)

add_executable(instruction_test instruction_test.c)
add_test(instruction_test instruction_test)
target_link_libraries(instruction_test seaboy cmocka)

add_executable(alu_test alu_test.c)
target_include_directories(alu_test PRIVATE "${CMAKE_SOURCE_DIR}/src")
add_test(alu_test alu_test)
target_link_libraries(alu_test seaboy cmocka)

add_executable(timer_test timer_test.c)
add_test(timer_test timer_test)
target_link_libraries(timer_test seaboy cmocka)

add_executable(cpu_test cpu_test.c)
add_test(NAME cpu_test COMMAND cpu_test WORKING_DIRECTORY "${PROJECT_TEST_DIR}/test-roms")
target_link_libraries(cpu_test seaboy cmocka)

add_executable(joypad_test joypad_test.c)
add_test(joypad_test joypad_test)
target_link_libraries(joypad_test seaboy cmocka)
