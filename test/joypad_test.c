#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>

#include "seaboy_joypad.h"
#include "seaboy_mmu.h"

static void reset_joypad(void) {
	mem_write8(JOYP_ADDRESS, QUERY_RESET);
	release_button(SEABOY_BTN_A);
	release_button(SEABOY_BTN_B);
	release_button(SEABOY_BTN_START);
	release_button(SEABOY_BTN_SELECT);
	release_direction(SEABOY_DIR_UP);
	release_direction(SEABOY_DIR_DOWN);
	release_direction(SEABOY_DIR_RIGHT);
	release_direction(SEABOY_DIR_LEFT);
}

static void test_button(enum seaboy_button btn, uint8_t expected_status) {
	reset_joypad();
	press_button(btn);

	mem_write8(JOYP_ADDRESS, QUERY_BUTTONS_STATE);
	assert_int_equal(expected_status, mem_read8(JOYP_ADDRESS));
}

static void test_directional(enum seaboy_direction dir, uint8_t expected_status) {
	reset_joypad();
	press_direction(dir);

	mem_write8(JOYP_ADDRESS, QUERY_DIRECTION_STATE);
	assert_int_equal(expected_status, mem_read8(JOYP_ADDRESS));
}

static void test_all_buttons(void **state) {
	(void) state; /* unused */
	test_button(SEABOY_BTN_A, 0xfe);
	test_button(SEABOY_BTN_B, 0xfd);
	test_button(SEABOY_BTN_SELECT, 0xfb);
	test_button(SEABOY_BTN_START, 0xf7);
}

static void test_all_directionals(void **state) {
	(void) state; /* unused */
	test_directional(SEABOY_DIR_RIGHT, 0xfe);
	test_directional(SEABOY_DIR_LEFT, 0xfd);
	test_directional(SEABOY_DIR_UP, 0xfb);
	test_directional(SEABOY_DIR_DOWN, 0xf7);
}

static void test_pair(enum seaboy_button btn, enum seaboy_direction dir, uint8_t expected_btn,
					  uint8_t expected_dir) {
	reset_joypad();
	press_button(btn);
	press_direction(dir);

	mem_write8(JOYP_ADDRESS, QUERY_DIRECTION_STATE);
	assert_int_equal(expected_dir, mem_read8(JOYP_ADDRESS));

	mem_write8(JOYP_ADDRESS, QUERY_BUTTONS_STATE);
	assert_int_equal(expected_btn, mem_read8(JOYP_ADDRESS));
}

static void test_all_pairs(void **state) {
	(void) state; /* unused */

	test_pair(SEABOY_BTN_A, SEABOY_DIR_RIGHT, 0xfe, 0xfe);
	test_pair(SEABOY_BTN_A, SEABOY_DIR_LEFT, 0xfe, 0xfd);
	test_pair(SEABOY_BTN_A, SEABOY_DIR_UP, 0xfe, 0xfb);
	test_pair(SEABOY_BTN_A, SEABOY_DIR_DOWN, 0xfe, 0xf7);

	test_pair(SEABOY_BTN_B, SEABOY_DIR_RIGHT, 0xfd, 0xfe);
	test_pair(SEABOY_BTN_B, SEABOY_DIR_LEFT, 0xfd, 0xfd);
	test_pair(SEABOY_BTN_B, SEABOY_DIR_UP, 0xfd, 0xfb);
	test_pair(SEABOY_BTN_B, SEABOY_DIR_DOWN, 0xfd, 0xf7);

	test_pair(SEABOY_BTN_SELECT, SEABOY_DIR_RIGHT, 0xfb, 0xfe);
	test_pair(SEABOY_BTN_SELECT, SEABOY_DIR_LEFT, 0xfb, 0xfd);
	test_pair(SEABOY_BTN_SELECT, SEABOY_DIR_UP, 0xfb, 0xfb);
	test_pair(SEABOY_BTN_SELECT, SEABOY_DIR_DOWN, 0xfb, 0xf7);

	test_pair(SEABOY_BTN_START, SEABOY_DIR_RIGHT, 0xf7, 0xfe);
	test_pair(SEABOY_BTN_START, SEABOY_DIR_LEFT, 0xf7, 0xfd);
	test_pair(SEABOY_BTN_START, SEABOY_DIR_UP, 0xf7, 0xfb);
	test_pair(SEABOY_BTN_START, SEABOY_DIR_DOWN, 0xf7, 0xf7);
}

int main(void) {
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_all_buttons),
		cmocka_unit_test(test_all_directionals),
		cmocka_unit_test(test_all_pairs),
	};
	return cmocka_run_group_tests(tests, NULL, NULL);
}
