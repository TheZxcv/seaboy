#ifndef SEABOY_INTERRUPTS
#define SEABOY_INTERRUPTS

/* TODO: enable when AlignConsecutiveMacros is available */
/* clang-format off */
/** INTERRUPT VECTORS **/
#define VBLANK_VECTOR   0x0040
#define LCD_STAT_VECTOR 0x0048
#define TIMER_VECTOR    0x0050
#define SERIAL_VECTOR   0x0058
#define JOYPAD_VECTOR   0x0060

/** MASKS **/
#define VBLANK_INT_MASK   0x01
#define LCD_STAT_INT_MASK 0x02
#define TIMER_INT_MASK    0x04
#define SERIAL_INT_MASK   0x08
#define JOYPAD_INT_MASK   0x10

#define VBLANK_INT_SHIFT   0
#define LCD_STAT_INT_SHIFT 1
#define TIMER_INT_SHIFT    2
#define SERIAL_INT_SHIFT   3
#define JOYPAD_INT_SHIFT   4

#define VBLANK_INT   VBLANK_INT_MASK
#define LCD_STAT_INT LCD_STAT_INT_MASK
#define TIMER_INT    TIMER_INT_MASK
#define SERIAL_INT   SERIAL_INT_MASK
#define JOYPAD_INT   JOYPAD_INT_MASK
/* clang-format on */

#endif // SEABOY_INTERRUPTS
