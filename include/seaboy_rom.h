#ifndef SEABOY_ROM
#define SEABOY_ROM

#include <stdbool.h>
#include <stdint.h>

#define HEADER_OFFSET 0x0134

/* all char arrays are *NOT* null-terminated */
struct rom_header_s {
	char title[11];
	char manufacturer[4];
	uint8_t cgb_flags;
	char licensee_code[2];
	uint8_t sgb_flags;
	uint8_t type;
	uint8_t rom_size;
	uint8_t ram_size;
	uint8_t japan_only;
	uint8_t licensee_code_old;
	uint8_t rom_version;
	uint8_t header_checksum;
	uint16_t checksum;
} __attribute__((packed));
typedef struct rom_header_s rom_header_t;

bool read_rom_header(const char *filename, rom_header_t *header);

enum MBC_version {
	NO_MBC,
	MBC1,
	MBC2,
	MBC3,
	MBC5,
	MBC6,
};

struct ROM_s {
	enum MBC_version mbc;
	int rom_banks;
	int ram_banks;
	bool has_rtc;
	bool has_rumble;
	bool has_sram;
	bool has_battery;
	uint8_t *rom;
	uint8_t *ram;
};
typedef struct ROM_s ROM_t;

ROM_t *read_rom(const char *filename);
bool save_savegame(const char *filename, ROM_t *rom);
bool load_savegame(const char *filename, ROM_t *rom);

#endif // SEABOY_ROM
