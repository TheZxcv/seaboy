#ifndef SEABOY_AUDIO_SOUND3_H
#define SEABOY_AUDIO_SOUND3_H

#include <stdint.h>

void sound3_init(unsigned long cycles);
void sound3_update(unsigned long cycles);
uint8_t sound3_get_sample(void);

#endif // SEABOY_AUDIO_SOUND3_H
