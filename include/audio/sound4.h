#ifndef SEABOY_AUDIO_SOUND4_H
#define SEABOY_AUDIO_SOUND4_H

#include <stdint.h>

void sound4_init(unsigned long cycles);
void sound4_update(unsigned long cycles);
uint8_t sound4_get_sample(void);

#endif // SEABOY_AUDIO_SOUND4_H
