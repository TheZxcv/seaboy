#ifndef SEABOY_AUDIO_SOUND1_H
#define SEABOY_AUDIO_SOUND1_H

#include <stdint.h>

void sound1_init(unsigned long cycles);
void sound1_update(unsigned long cycles);
uint8_t sound1_get_sample(void);

#endif // SEABOY_AUDIO_SOUND1_H
