#ifndef SEABOY_AUDIO_SOUND2_H
#define SEABOY_AUDIO_SOUND2_H

#include <stdint.h>

void sound2_init(unsigned long cycles);
void sound2_update(unsigned long cycles);
uint8_t sound2_get_sample(void);

#endif // SEABOY_AUDIO_SOUND2_H
