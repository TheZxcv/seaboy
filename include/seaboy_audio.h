#ifndef SEABOY_AUDIO_H
#define SEABOY_AUDIO_H

#include <stddef.h>
#include <stdint.h>

#include "seaboy_core.h"

/* clang-format off */
#define CHANNELS            2
#define SAMPLE_RATE     44100

#define FRAMES_PER_BUFFER (1024U)
#define SAMPLE_BUFFER_SIZE (CHANNELS*FRAMES_PER_BUFFER)

// Sound 1: square
#define NR10_ADDRESS             UINT16_C(0xff10)
#define NR10_SWEEP_TIME_MASK     UINT8_C(0x70)
#define NR10_SWEEP_DECREASE_MASK UINT8_C(0x08)
#define NR10_SWEEP_SHIFT_MASK    UINT8_C(0x07)

#define NR11_ADDRESS UINT16_C(0xff11)
#define NR12_ADDRESS UINT16_C(0xff12)
#define NR13_ADDRESS UINT16_C(0xff13)
#define NR14_ADDRESS UINT16_C(0xff14)

// Sound 2: square 2
#define NR21_ADDRESS UINT16_C(0xff16)
#define NR22_ADDRESS UINT16_C(0xff17)
#define NR23_ADDRESS UINT16_C(0xff18)
#define NR24_ADDRESS UINT16_C(0xff19)

// Sound 3: wave
#define NR30_ADDRESS UINT16_C(0xff1a)
#define NR31_ADDRESS UINT16_C(0xff1b)
#define NR32_ADDRESS UINT16_C(0xff1c)
#define NR33_ADDRESS UINT16_C(0xff1d)
#define NR34_ADDRESS UINT16_C(0xff1e)

// Sound 4: white-noise
#define NR41_ADDRESS UINT16_C(0xff20)
#define NR42_ADDRESS UINT16_C(0xff21)
#define NR43_ADDRESS UINT16_C(0xff22)
#define NR44_ADDRESS UINT16_C(0xff23)

// Control
#define NR50_ADDRESS             UINT16_C(0xff24)
#define NR50_VIN_SO2_ENABLE_MASK UINT8_C(0x80)
#define NR50_SO2_LEVEL_MASK      UINT8_C(0x70)
#define NR50_VIN_SO1_ENABLE_MASK UINT8_C(0x08)
#define NR50_SO1_LEVEL_MASK      UINT8_C(0x07)

#define NR51_ADDRESS            UINT16_C(0xff25)
#define NR51_SOUND4_TO_SO2_MASK UINT8_C(0x80)
#define NR51_SOUND3_TO_SO2_MASK UINT8_C(0x40)
#define NR51_SOUND2_TO_SO2_MASK UINT8_C(0x20)
#define NR51_SOUND1_TO_SO2_MASK UINT8_C(0x10)
#define NR51_SOUND4_TO_SO1_MASK UINT8_C(0x08)
#define NR51_SOUND3_TO_SO1_MASK UINT8_C(0x04)
#define NR51_SOUND2_TO_SO1_MASK UINT8_C(0x02)
#define NR51_SOUND1_TO_SO1_MASK UINT8_C(0x01)

#define NR52_ADDRESS            UINT16_C(0xff26)
#define NR52_ENABLE_ALL_MASK    UINT8_C(0x80)
#define NR52_SOUND4_ENABLE_MASK UINT8_C(0x08)
#define NR52_SOUND3_ENABLE_MASK UINT8_C(0x04)
#define NR52_SOUND2_ENABLE_MASK UINT8_C(0x02)
#define NR52_SOUND1_ENABLE_MASK UINT8_C(0x01)
/* clang-format on */

#ifdef __cplusplus
extern "C" {
#endif
void init_audio(unsigned long cycles);
void audio_update(unsigned long cycles);

typedef void (*play_sample_callback_t)(int16_t *samples, size_t length);
void set_audio_callback(play_sample_callback_t);

extern uint8_t duty_cycles[4][8];
#ifdef __cplusplus
}
#endif

#endif // SEABOY_AUDIO_H
