#ifndef SEABOY_H
#define SEABOY_H

#include <stdbool.h>
#include <stdint.h>

#include "seaboy_joypad.h"
#include "seaboy_video.h"

#ifdef __cplusplus
extern "C" {
#endif

void seaboy_init(void);

void print_regs(void);

int opcode_len(uint8_t op);

bool load_rom(const char *filename);

void seaboy_run(void);

void seaboy_step(void);

void seaboy_run_till_vblank(void);

void set_frameskip(int);

uint8_t mem_read8(uint16_t);

#ifdef __cplusplus
}
#endif

#endif // SEABOY_H
