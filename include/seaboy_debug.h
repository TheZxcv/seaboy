#ifndef SEABOY_SEABOY_DEBUG_H
#define SEABOY_SEABOY_DEBUG_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

bool disass(const uint8_t *opcodes, char *buffer, size_t buflen);

#endif // SEABOY_DEBUG_H
