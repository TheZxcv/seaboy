#ifndef SEABOY_VIDEO
#define SEABOY_VIDEO

#include <stdbool.h>
#include <stdint.h>

/* TODO: enable when AlignConsecutiveMacros is available */
/* clang-format off */
/** LCD CONTROLLER REGISTERS **/
#define LCDC_ADDRESS               UINT16_C(0xff40)
#define LCDC_ENABLE_MASK           UINT8_C(0x80)
#define LCDC_WINDOW_TILE_MAP_MASK  UINT8_C(0x40)
#define LCDC_WINDOW_ENABLE_MASK    UINT8_C(0x20)
#define LCDC_BG_TILE_DATA_MASK     UINT8_C(0x10)
#define LCDC_BG_TILE_MAP_MASK      UINT8_C(0x08)
#define LCDC_OBJ_SIZE_MASK         UINT8_C(0x04)
#define LCDC_OBJ_ENABLE_MASK       UINT8_C(0x02)
#define LCDC_BG_ENABLE_MASK        UINT8_C(0x01)

#define STAT_ADDRESS                   UINT16_C(0xff41)
#define STAT_LY_COINCIDENCE_IE_MASK    UINT8_C(0x40)
#define STAT_MODE2_IE_MASK             UINT8_C(0x20)
#define STAT_MODE1_IE_MASK             UINT8_C(0x10)
#define STAT_MODE0_IE_MASK             UINT8_C(0x08)
#define STAT_LY_COINCIDENCE_FLAG_MASK  UINT8_C(0x04)
#define STAT_MODE_MASK                 UINT8_C(0x03)

#define SCY_ADDRESS   UINT16_C(0xff42)
#define SCX_ADDRESS   UINT16_C(0xff43)
#define LY_ADDRESS    UINT16_C(0xff44)
#define LYC_ADDRESS   UINT16_C(0xff45)
#define WY_ADDRESS    UINT16_C(0xff4a)
#define WX_ADDRESS    UINT16_C(0xff4b)
#define BGP_ADDRESS   UINT16_C(0xff47)
#define OBP0_ADDRESS  UINT16_C(0xff48)
#define OBP1_ADDRESS  UINT16_C(0xff49)

#define LY_VBLANK_START_LINE  144
#define LY_VBLANK_END_LINE    154

/** OBJECTS **/
#define OAM_START_ADDRESS  UINT16_C(0xfe00)
#define OAM_END_ADDRESS    UINT16_C(0xfe9c)
#define OBJ_SIZE           UINT16_C(4)

#define OBJ_Y_OFFSET  UINT8_C(0x10)
#define OBJ_X_OFFSET  UINT8_C(0x08)

#define OBJ_ATTR_PRIORITY_MASK  UINT8_C(0x80)
#define OBJ_ATTR_VFLIP_MASK     UINT8_C(0x40)
#define OBJ_ATTR_HFLIP_MASK     UINT8_C(0x20)
#define OBJ_ATTR_PALETTE_MASK   UINT8_C(0x10)

#define TILE_SIZE     16
#define TILE_WIDTH     8
#define TILE_HEIGHT    8
#define TILE_MAP_ROWS 32
#define TILE_MAP_COLS 32

/** SCREEN SIZE **/
#define HEIGHT_DOTS 144
#define WIDTH_DOTS  160

#define VIDEORAM_HEIGHT  256
#define VIDEORAM_WIDTH   256

/** TIMING **/
#define VBLANK_CYCLES  70224
#define HBLANK_CYCLES  456
/* clang-format on */

#ifdef __cplusplus
extern "C" {
#endif

void get_screen(uint8_t **);
void get_vram(uint8_t **);

void init_video_driver(unsigned long starting_cycle);
void video_driver(unsigned long cycles);

bool redraw_display(void);

#ifdef __cplusplus
}
#endif

#endif // SEABOY_VIDEO
