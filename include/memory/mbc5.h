#ifndef MBC5_H
#define MBC5_H

#include <stdint.h>

/* TODO: enable when AlignConsecutiveMacros is available */
/* clang-format off */
#define RAM_ENABLE_START_ADDRESS     UINT16_C(0x0000)
#define RAM_ENABLE_END_ADDRESS       UINT16_C(0x1fff)

#define MBC5_BANK_LOW_START_ADDRESS  UINT16_C(0x2000)
#define MBC5_BANK_LOW_END_ADDRESS    UINT16_C(0x2fff)

#define MBC5_BANK_HIGH_START_ADDRESS UINT16_C(0x3000)
#define MBC5_BANK_HIGH_END_ADDRESS   UINT16_C(0x3fff)

#define MBC5_BANK_RAM_START_ADDRESS  UINT16_C(0x4000)
#define MBC5_BANK_RAM_END_ADDRESS    UINT16_C(0x5fff)
/* clang-format on */

void mbc5_write8(uint16_t, uint8_t);

#endif // MBC5_H
