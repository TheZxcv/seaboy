#ifndef MBC1_H
#define MBC1_H

#include <stdint.h>

/* TODO: enable when AlignConsecutiveMacros is available */
/* clang-format off */
#define RAM_ENABLE_START_ADDRESS  UINT16_C(0x0000)
#define RAM_ENABLE_END_ADDRESS    UINT16_C(0x1fff)

#define MBC1_BANK1_START_ADDRESS  UINT16_C(0x2000)
#define MBC1_BANK1_END_ADDRESS    UINT16_C(0x3fff)

#define MBC1_BANK2_START_ADDRESS  UINT16_C(0x4000)
#define MBC1_BANK2_END_ADDRESS    UINT16_C(0x5fff)

#define MBC1_MODE_START_ADDRESS   UINT16_C(0x6000)
#define MBC1_MODE_END_ADDRESS     UINT16_C(0x7fff)
/* clang-format on */

void mbc1_write8(uint16_t, uint8_t);

#endif // MBC1_H
