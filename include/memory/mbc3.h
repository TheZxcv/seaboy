#ifndef MBC3_H
#define MBC3_H

#include <stdint.h>

/* TODO: enable when AlignConsecutiveMacros is available */
/* clang-format off */
#define RAM_ENABLE_START_ADDRESS     UINT16_C(0x0000)
#define RAM_ENABLE_END_ADDRESS       UINT16_C(0x1fff)

#define MBC3_BANK_ROM_START_ADDRESS  UINT16_C(0x2000)
#define MBC3_BANK_ROM_END_ADDRESS    UINT16_C(0x3fff)

#define MBC3_BANK_RAM_START_ADDRESS  UINT16_C(0x4000)
#define MBC3_BANK_RAM_END_ADDRESS    UINT16_C(0x5fff)
/* clang-format on */

void mbc3_write8(uint16_t, uint8_t);

#endif // MBC3_H
