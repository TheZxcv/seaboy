#ifndef SEABOY_CORE_H
#define SEABOY_CORE_H

#include <stdbool.h>
#include <stdint.h>

#define DMG_CPU_FREQ (4194304U)
#define CPU_FREQ (DMG_CPU_FREQ)

extern bool IME; // Interrupt Master Enable
enum seaboy_cpustate {
	POWERED_OFF, // nothing is currently running
	RUNNING,     // a ROM is running
	HALTED       // `HALT` has been executed. Waiting for an interrupt.
};
extern enum seaboy_cpustate CPU_STATE;

#define FLAG_C 0x10
#define FLAG_H 0x20
#define FLAG_N 0x40
#define FLAG_Z 0x80

#define FLAG_C_SHIFT 4
#define FLAG_H_SHIFT 5
#define FLAG_N_SHIFT 6
#define FLAG_Z_SHIFT 7

#define SETF(flag, regs) (regs.F |= flag)
#define CLEARF(flag, regs) (regs.F &= 0xff & (~flag))
#define FLIPF(flag, regs) (regs.F ^= flag)
#define GETF(flag, regs) ((regs.F & flag) >> flag##_SHIFT)
#define COPY_TOF(flag, regs, cond) (regs.F = (regs.F & (0xff & (~flag))) | ((cond) ? flag : 0))

#define SET_PAIR(regH, regL, imm) \
	((regL) = ((imm) &UINT8_C(0xFF)), (regH) = ((imm) >> UINT8_C(8)) & UINT8_C(0xff))

struct seaboy_regs {
	uint8_t A;
	uint8_t F;
	uint8_t B;
	uint8_t C;
	uint8_t D;
	uint8_t E;
	uint8_t H;
	uint8_t L;
	uint16_t SP;
	uint16_t PC;
};

#define GET_REG16(opcode) (((opcode) &0x30) >> 4)
#define GET_REG8_DST(opcode) (((opcode) &0x38) >> 3)
#define GET_REG8_SRC(opcode) ((opcode) &0x07)
#define GET_CONDITION(opcode) (((opcode) &0x18) >> 3)
#define GET_ALU_OP(opcode) (((opcode) &0x38) >> 3)
#define GET_RST_NUM(opcode) (((opcode) &0x38) >> 3)
#define GET_CB_NUM(opcode) (((opcode) &0x38) >> 3)
#define GET_CB_REG8(opcode) ((opcode) &0x07)

struct opcode_s {
	uint8_t main;
	uint8_t low;
	uint8_t high;
};
typedef struct opcode_s opcode_t;
typedef int (*instruction_t)(opcode_t);

instruction_t decode(opcode_t opcode);
struct seaboy_regs *get_regs(void);

#endif // SEABOY_CORE_H
