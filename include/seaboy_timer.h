#ifndef SEABOY_TIMER
#define SEABOY_TIMER

#define TIMER_ENABLED_MASK 0x04
#define TIMER_CLOCK_SELECTOR_MASK 0x03

void init_timer(unsigned long starting_cycle);

void timer(unsigned long cycles);

#endif // SEABOY_TIMER
