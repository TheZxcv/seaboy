#ifndef SEABOY_UTILS_H
#define SEABOY_UTILS_H

#define TO_WORD(high, low) ((uint16_t)((((uint16_t)(high)) << UINT16_C(8)) | ((uint16_t)(low))))
#define WORD_HIBYTE(word) ((uint8_t)(((word) >> UINT16_C(8)) & UINT16_C(0xff)))
#define WORD_LOBYTE(word) ((uint8_t)((word) &UINT16_C(0xff)))

#define BYTE_HINIBBLE(byte) ((uint8_t)(((byte) >> UINT8_C(4)) & UINT8_C(0x0f)))
#define BYTE_LONIBBLE(byte) ((uint8_t)((byte) &UINT8_C(0x0f)))

#define BYTE_SIGN_EXT(byte) (((byte) &0x80) ? (0xffffff00 | (byte)) : (byte))

#define die()                                  \
	do {                                       \
		printf("%s:%d\n", __FILE__, __LINE__); \
		abort();                               \
	} while (0)

#endif // SEABOY_UTILS_H
