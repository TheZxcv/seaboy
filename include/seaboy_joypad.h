#ifndef SEABOY_JOYPAD
#define SEABOY_JOYPAD

#include <stdint.h>

/* TODO: enable when AlignConsecutiveMacros is available */
/* clang-format off */
#define QUERY_RESET           0x30
#define QUERY_BUTTONS_STATE   0x10
#define QUERY_DIRECTION_STATE 0x20
/* clang-format on */

enum seaboy_button {
	SEABOY_BTN_A = 0x01,
	SEABOY_BTN_B = 0x02,
	SEABOY_BTN_SELECT = 0x04,
	SEABOY_BTN_START = 0x08,
};

enum seaboy_direction {
	SEABOY_DIR_RIGHT = 0x01,
	SEABOY_DIR_LEFT = 0x02,
	SEABOY_DIR_UP = 0x04,
	SEABOY_DIR_DOWN = 0x08,
};

uint8_t get_buttons_state(void);
uint8_t get_direction_state(void);
uint8_t query_input_state(uint8_t query);

#ifdef __cplusplus
extern "C" {
#endif
void press_button(enum seaboy_button);
void release_button(enum seaboy_button);

void press_direction(enum seaboy_direction);
void release_direction(enum seaboy_direction);
#ifdef __cplusplus
}
#endif

#endif // SEABOY_JOYPAD
