#ifndef SEABOY_MMU
#define SEABOY_MMU

#include "seaboy_rom.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

/* clang-format off */
/** OFFSETS **/
#define ENTRY_POINT           UINT16_C(0x0100)
#define ROM_BANK0_OFFSET      UINT16_C(0x0000)
#define ROM_SWITCHBANK_OFFSET UINT16_C(0x4000)
#define RAM_VIDEO_OFFSET      UINT16_C(0x8000)
#define RAM_SWITCHBANK_OFFSET UINT16_C(0xA000)
#define RAM_OFFSET            UINT16_C(0xC000)
#define RAM_ECHO_OFFSET       UINT16_C(0xE000)
#define RAM_UNUSABLE0_OFFSET  UINT16_C(0xE000)
#define RAM_OAM_OFFSET        UINT16_C(0xFE00)
#define RAM_UNUSABLE1_OFFSET  UINT16_C(0xFEA0)
#define RAM_IOPORTS_OFFSET    UINT16_C(0xFF00)
#define STACK_BOTTOM_OFFSET   UINT16_C(0xFF80)
#define STACK_TOP_OFFSET      UINT16_C(0xFFFE)

/** SIZES **/
#define ROM_BANK0_SIZE       16*1024 // 16KiB
#define ROM_SWITCHBANK_SIZE  16*1024 // 16KiB
#define RAM_VIDEO_SIZE        8*1024 // 8KiB
#define RAM_SWITCHBANK_SIZE   8*1024 // 8KiB
#define RAM_SIZE              8*1024 // 8KiB
#define RAM_OAM_SIZE         40*32*8 // 40 * 32B
#define RAM_IOPORTS_SIZE         128 // 128B
#define STACK_SIZE               127 // 128B - 1B
/* clang-format on */

struct gb_memory_s {
	ROM_t *cartridge;
	uint8_t *romBanks;
	uint8_t *romBank0;
	uint8_t *romSwitchBank;
	uint8_t ramVideo[RAM_VIDEO_SIZE];
	uint8_t *ramSwitchBank;
	uint8_t *ramBanks;
	uint8_t ram[RAM_SIZE];
	uint8_t ramOAM[RAM_OAM_SIZE];
	uint8_t ioports[RAM_IOPORTS_SIZE];
	uint8_t stack[STACK_SIZE + 1];
};
typedef struct gb_memory_s gb_memory_t;

/* clang-format off */
/** LOCATIONS **/
#define IE_ADDRESS           UINT16_C(0xffff)
#define IF_ADDRESS           UINT16_C(0xff0f)
#define DMA_ADDRESS          UINT16_C(0xff46)
#define SERIAL_DATA_ADDRESS  UINT16_C(0xff01)
#define DIV_ADDRESS          UINT16_C(0xff04)
#define TIMA_ADDRESS         UINT16_C(0xff05)
#define TMA_ADDRESS          UINT16_C(0xff06)
#define TAC_ADDRESS          UINT16_C(0xff07)
#define JOYP_ADDRESS         UINT16_C(0xff00)

#define DMA_BASE_ADDRESS     UINT16_C(0xfe00)
#define DMA_TRANSFER_SIZE    UINT16_C(0xa0)
/* clang-format on */

uint8_t mem_read8(uint16_t address);
uint16_t mem_read16(uint16_t address);

bool mem_write8(uint16_t address, uint8_t value);
bool mem_write16(uint16_t address, uint16_t value);

/* Given the address `address`, returns in `mem_block` the pointer to
 * the beginning of the memory block in which `address` belongs and
 * returns its offset from it.
 */
uint16_t resolve_address(uint16_t address, uint8_t **mem_block);

gb_memory_t *get_memory(void);
bool plug_rom(ROM_t *rom);

/* `callback` gets called any time there is a write to the serial
 * port.
 */
void set_serial_callback(void (*callback)(uint8_t));

#endif // SEABOY_MMU
