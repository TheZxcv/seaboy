#include "audio.h"

#include <alsa/asoundlib.h>
#include <portaudio.h>

static PaStreamParameters outputParameters;
static PaStream *stream;

void null_error_handler(const char *, int, const char *, int, const char *, ...) {
}

void setup_audio() {
	// disable ALSA error messages
	snd_lib_error_set_handler(null_error_handler);

	PaError err = Pa_Initialize();
	outputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
	outputParameters.channelCount = CHANNELS;
	outputParameters.sampleFormat = paInt16;
	outputParameters.suggestedLatency =
			Pa_GetDeviceInfo(outputParameters.device)->defaultLowOutputLatency;
	outputParameters.hostApiSpecificStreamInfo = NULL;

	err = Pa_OpenStream(&stream, NULL, &outputParameters, SAMPLE_RATE, FRAMES_PER_BUFFER, paClipOff,
						NULL /* no callback */, NULL /* no userdata */);
	err = Pa_StartStream(stream);
}

void teardown_audio() {
	PaError err = Pa_StopStream(stream);
	err = Pa_CloseStream(stream);
	Pa_Terminate();
}

void play_sample(int16_t *samples, size_t length) {
	PaError err = Pa_WriteStream(stream, samples, length);
}
