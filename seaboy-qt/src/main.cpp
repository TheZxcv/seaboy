#include <QApplication>

#include "audio.h"
#include "seaboy.h"
#include "window.h"

int main(int argc, char *argv[]) {
	seaboy_init();
	if (argc < 1)
		return EXIT_FAILURE;
	if (!load_rom(argv[1]))
		return EXIT_FAILURE;

	setup_audio();
	set_audio_callback(play_sample);
	QApplication app(argc, argv);

	Window window;
	window.show();

	int retval = app.exec();
	teardown_audio();
	return retval;
}
