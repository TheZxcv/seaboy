#include "window.h"

#include "seaboy.h"

#include <QApplication>
#include <QPainter>
#include <iostream>

Window::Window(QWidget *parent)
		: QWidget(parent), m_board(QImage(WIDTH_DOTS, HEIGHT_DOTS, QImage::Format_ARGB32)),
		  m_vram(QImage(VIDEORAM_WIDTH, VIDEORAM_HEIGHT, QImage::Format_ARGB32)), frameCount(0),
		  m_frameskip(0), m_drawvram(false) {
	std::fill(std::begin(m_buffer), std::end(m_buffer), palette[0]);
	std::fill(std::begin(m_vram_buffer), std::end(m_vram_buffer), palette[0]);

	resize(WIDTH_DOTS, HEIGHT_DOTS);
	init();
	startTimer(0);
}

void Window::init() {
	palette[0] = QColor(255, 255, 255).rgb();
	palette[1] = QColor(170, 170, 170).rgb();
	palette[2] = QColor(90, 90, 90).rgb();
	palette[3] = QColor(0, 0, 0).rgb();
	m_board.fill(palette[0]);
	frameTime.start();
}

void Window::paintEvent(QPaintEvent *e) {
	Q_UNUSED(e);

	QPainter qp(this);
	if (m_drawvram) {
		qp.setWindow(QRect(0, 0, WIDTH_DOTS + VIDEORAM_WIDTH, VIDEORAM_HEIGHT));
	} else {
		qp.setWindow(QRect(0, 0, WIDTH_DOTS, HEIGHT_DOTS));
	}
	qp.save();

	if (m_drawvram) {
		QImage vimage{ reinterpret_cast<uchar *>(m_vram_buffer), VIDEORAM_WIDTH, VIDEORAM_HEIGHT,
					   QImage::Format_RGB32 };
		m_vram.swap(vimage);
		qp.drawImage(QPoint(WIDTH_DOTS + 1, 0), m_vram);

		QPen pen; // creates a default pen

		pen.setStyle(Qt::DashLine);
		pen.setWidth(1);
		pen.setBrush(Qt::blue);
		qp.setPen(pen);

		uint8_t scy = mem_read8(0xff42);
		uint8_t scx = mem_read8(0xff43);

		int origX = WIDTH_DOTS + 1;
		int origY = 0;

		qp.drawRect(origX + scx, origY + scy, WIDTH_DOTS, HEIGHT_DOTS);
		qp.drawRect(origX + scx, origY + scy - VIDEORAM_HEIGHT - 1, WIDTH_DOTS, HEIGHT_DOTS);
		qp.drawRect(origX + scx - VIDEORAM_WIDTH - 1, origY + scy, WIDTH_DOTS, HEIGHT_DOTS);
		qp.drawRect(origX + scx - VIDEORAM_WIDTH - 1, origY + scy - VIDEORAM_HEIGHT - 1, WIDTH_DOTS,
					HEIGHT_DOTS);

		qp.fillRect(QRect(0, 0, origX, VIDEORAM_HEIGHT), Qt::black);
	}

	QImage image{ reinterpret_cast<uchar *>(m_buffer), WIDTH_DOTS, HEIGHT_DOTS,
				  QImage::Format_RGB32 };
	m_board.swap(image);
	qp.drawImage(QPoint(0, 0), m_board);

	qp.restore();
	drawFPSCounter(qp);
}

void Window::drawFPSCounter(QPainter &painter) {
	++frameCount;
	if (frameTime.elapsed() >= 1000) {
		m_fps = frameCount;
		frameCount = 0;
		frameTime.restart();
	}
	QString str = QString("FPS: %1").arg(m_fps);

	QPen pen{};
	pen.setBrush(Qt::black);
	painter.setPen(pen);
	QFont font = painter.font();
	int text_height = (2 * HEIGHT_DOTS / 100) * (height() / HEIGHT_DOTS);
	font.setPixelSize(text_height);
	painter.setFont(font);

	QFontMetrics fMetrics = painter.fontMetrics();
	QSize sz = fMetrics.size(Qt::TextSingleLine, str);
	painter.drawText(0, sz.height(), str);
}

void Window::timerEvent(QTimerEvent *e) {
	Q_UNUSED(e);

	seaboy_run_till_vblank();
	uint8_t *screen;
	get_screen(&screen);
	for (int i = 0; i < WIDTH_DOTS * HEIGHT_DOTS; i++) {
		m_buffer[i] = palette[screen[i]];
	}

	if (m_drawvram) {
		uint8_t *vram;
		get_vram(&vram);
		for (int i = 0; i < VIDEORAM_WIDTH * VIDEORAM_HEIGHT; i++) {
			m_vram_buffer[i] = palette[vram[i]];
		}
	}

	repaint();
}

void Window::keyPressEvent(QKeyEvent *e) {
	int key = e->key();

	if (key == Qt::Key_Q)
		QApplication::instance()->quit();
	else if (key == Qt::Key_Plus)
		set_frameskip(++m_frameskip);
	else if (key == Qt::Key_Minus && m_frameskip > 0)
		set_frameskip(--m_frameskip);
	else if (key == Qt::Key_Space)
		set_frameskip(10);

	if (!e->isAutoRepeat()) {
		if (key == Qt::Key_Return)
			press_button(SEABOY_BTN_START);
		else if (key == Qt::Key_Shift)
			press_button(SEABOY_BTN_SELECT);
		else if (key == Qt::Key_A)
			press_button(SEABOY_BTN_A);
		else if (key == Qt::Key_B)
			press_button(SEABOY_BTN_B);

		if (key == Qt::Key_Up)
			press_direction(SEABOY_DIR_UP);
		else if (key == Qt::Key_Down)
			press_direction(SEABOY_DIR_DOWN);
		else if (key == Qt::Key_Left)
			press_direction(SEABOY_DIR_LEFT);
		else if (key == Qt::Key_Right)
			press_direction(SEABOY_DIR_RIGHT);
	}

	QWidget::keyPressEvent(e);
}

void Window::keyReleaseEvent(QKeyEvent *e) {
	int key = e->key();

	if (key == Qt::Key_Space)
		set_frameskip(m_frameskip);

	if (!e->isAutoRepeat()) {
		if (key == Qt::Key_Return)
			release_button(SEABOY_BTN_START);
		else if (key == Qt::Key_Shift)
			release_button(SEABOY_BTN_SELECT);
		else if (key == Qt::Key_A)
			release_button(SEABOY_BTN_A);
		else if (key == Qt::Key_B)
			release_button(SEABOY_BTN_B);

		if (key == Qt::Key_Up)
			release_direction(SEABOY_DIR_UP);
		else if (key == Qt::Key_Down)
			release_direction(SEABOY_DIR_DOWN);
		else if (key == Qt::Key_Left)
			release_direction(SEABOY_DIR_LEFT);
		else if (key == Qt::Key_Right)
			release_direction(SEABOY_DIR_RIGHT);
	}

	QWidget::keyReleaseEvent(e);
}
