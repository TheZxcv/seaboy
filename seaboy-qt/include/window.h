#ifndef WINDOW_H
#define WINDOW_H

#include <QKeyEvent>
#include <QTime>
#include <QWidget>

#include "seaboy.h"

class Window : public QWidget {
public:
	explicit Window(QWidget *parent = nullptr);

protected:
	void paintEvent(QPaintEvent *);
	void timerEvent(QTimerEvent *);
	void keyPressEvent(QKeyEvent *);
	void keyReleaseEvent(QKeyEvent *);

private slots:
	void updateScaline(int, uint8_t *);

private:
	bool m_drawvram;
	QImage m_board;
	QImage m_vram;
	quint32 m_buffer[WIDTH_DOTS * HEIGHT_DOTS];
	quint32 m_vram_buffer[VIDEORAM_WIDTH * VIDEORAM_HEIGHT];
	QRgb palette[4];
	QTime frameTime;
	long frameCount;
	double m_fps = 0;
	int m_frameskip;
	void init();
	void drawFPSCounter(QPainter &painter);
};

#endif // WINDOW_H
