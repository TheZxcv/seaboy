#ifndef AUDIO_H
#define AUDIO_H

#include "seaboy_audio.h"

void setup_audio();

void teardown_audio();

void play_sample(int16_t *samples, size_t length);

#endif // AUDIO_H
