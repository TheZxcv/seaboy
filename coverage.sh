#!/bin/sh
set -e
lcov -q --directory . --capture --initial  -o coverage.base
lcov -q --directory . --capture -o coverage.run
lcov -q --directory . --add-tracefile coverage.base --add-tracefile coverage.run -o coverage.unfiltered
lcov -q --directory . --remove coverage.unfiltered \
	"/usr/include/*" \
	"/usr/local/include/*" \
	"*/test/*.c" \
	-o coverage.total
lcov --summary coverage.total
