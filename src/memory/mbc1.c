#include "memory/mbc1.h"
#include "seaboy_mmu.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

enum MBCBankingMode {
	MBC_ROM_MODE,
	MBC_RAM_MODE,
};

static int bank_reg1 = 1;
static int bank_reg2 = 0;

static int rom_bank0 = 0;
static int rom_bank = 1;
static int ram_bank = 0;
static bool ram_enabled = false;
static enum MBCBankingMode banking_mode = MBC_ROM_MODE;

static void update_rom_bank(void) {
	gb_memory_t *mem = get_memory();
	rom_bank = (bank_reg2 << 5) | bank_reg1;
	rom_bank %= mem->cartridge->rom_banks;
	mem->romSwitchBank = &mem->romBanks[rom_bank * ROM_SWITCHBANK_SIZE];
}

void mbc1_write8(uint16_t address, uint8_t value) {
	gb_memory_t *mem = get_memory();
	if (RAM_ENABLE_START_ADDRESS <= address && address <= RAM_ENABLE_END_ADDRESS) {
		ram_enabled = value & 0x0a;
	} else if (MBC1_BANK1_START_ADDRESS <= address && address <= MBC1_BANK1_END_ADDRESS) {
		value &= 0x1f; // MBC2 only supports up to 0x0f
		if (value == 0)
			value = 0x01;
		bank_reg1 = value;
		update_rom_bank();
	} else if (MBC1_BANK2_START_ADDRESS <= address && address <= MBC1_BANK2_END_ADDRESS) {
		if (banking_mode == MBC_RAM_MODE) {
			// RAM banking
			ram_bank = value & 0x03;
			ram_bank %= mem->cartridge->ram_banks;
			mem->ramSwitchBank = &mem->ramBanks[ram_bank * RAM_SWITCHBANK_SIZE];

			rom_bank0 = (value & 0x03) << 5;
			mem->romBank0 = &mem->romBanks[rom_bank0 * ROM_SWITCHBANK_SIZE];
		} else {
			// ROM banking
			bank_reg2 = value & 0x03;
			update_rom_bank();
		}
	} else if (MBC1_MODE_START_ADDRESS <= address && address <= MBC1_MODE_END_ADDRESS) {
		banking_mode = value & 0x01 ? MBC_RAM_MODE : MBC_ROM_MODE;
		if (banking_mode == MBC_ROM_MODE) {
			mem->ramSwitchBank = &mem->ramBanks[0];
			ram_bank = 0;
		} else {
			bank_reg2 = 0;
			update_rom_bank();
		}
	}
}
