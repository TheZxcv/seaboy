#include "memory/mbc3.h"
#include "seaboy_mmu.h"

#include <stdbool.h>
#include <stddef.h>

static int rom_bank = 1;
static int ram_bank = 0;
static bool ram_enabled = false;

void mbc3_write8(uint16_t address, uint8_t value) {
	gb_memory_t *mem = get_memory();
	if (RAM_ENABLE_START_ADDRESS <= address && address <= RAM_ENABLE_END_ADDRESS) {
		ram_enabled = value & 0x0a;
	} else if (MBC3_BANK_ROM_START_ADDRESS <= address && address <= MBC3_BANK_ROM_END_ADDRESS) {
		value &= 0x7f;
		rom_bank = value;
		ram_bank %= mem->cartridge->rom_banks;
		mem->romSwitchBank = &mem->romBanks[rom_bank * ROM_SWITCHBANK_SIZE];
	} else if (MBC3_BANK_RAM_START_ADDRESS <= address && address <= MBC3_BANK_RAM_END_ADDRESS) {
		if (value <= 0x07) {
			ram_bank = value & 0x07;
		} else if (0x08 >= value && value <= 0x0c) {
			// map RTC register
			ram_bank = 8;
		}
		ram_bank %= mem->cartridge->ram_banks;
		mem->ramSwitchBank = &mem->ramBanks[ram_bank * RAM_SWITCHBANK_SIZE];
	}
}
