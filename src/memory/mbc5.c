#include "memory/mbc5.h"
#include "seaboy_mmu.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

static int bank_low = 1;
static int bank_high = 0;

static int rom_bank = 1;
static int ram_bank = 0;
static bool ram_enabled = false;

static void update_rom_bank(void) {
	gb_memory_t *mem = get_memory();
	rom_bank = (bank_high << 8) | bank_low;
	rom_bank %= mem->cartridge->rom_banks;
	mem->romSwitchBank = &mem->romBanks[rom_bank * ROM_SWITCHBANK_SIZE];
}

void mbc5_write8(uint16_t address, uint8_t value) {
	gb_memory_t *mem = get_memory();
	if (RAM_ENABLE_START_ADDRESS <= address && address <= RAM_ENABLE_END_ADDRESS) {
		ram_enabled = value & 0x0a;
	} else if (MBC5_BANK_LOW_START_ADDRESS <= address && address <= MBC5_BANK_LOW_END_ADDRESS) {
		bank_low = value;
		update_rom_bank();
	} else if (MBC5_BANK_HIGH_START_ADDRESS <= address && address <= MBC5_BANK_HIGH_END_ADDRESS) {
		bank_high = value & 0x01;
		update_rom_bank();
	} else if (MBC5_BANK_RAM_START_ADDRESS <= address && address <= MBC5_BANK_RAM_END_ADDRESS) {
		if (value <= 0x0f) {
			ram_bank = value & 0x0f;
		} else {
			// map RTC register
			ram_bank = 0x10;
		}
		ram_bank %= mem->cartridge->ram_banks;
		mem->ramSwitchBank = &mem->ramBanks[ram_bank * RAM_SWITCHBANK_SIZE];
	}
}
