#include "audio/sound1.h"
#include "seaboy_audio.h"
#include "seaboy_mmu.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

#define SHADOW2SELECTED(shadow) (4 * (2048 - (shadow)))

static unsigned long last_update_length = 0;
static unsigned long last_update_duty = 0;
static unsigned long last_update_envelope = 0;
static unsigned long last_update_sweep = 0;

static bool enabled = false;
static int selected_length = 0;
static int selected_duty = 0;
static uint8_t curr_volume = 0;
static unsigned long envelope_len = 0;
static bool envelope_increase = false;
static unsigned long shadow_freq = 0;
static unsigned long selected_period = 0;

static unsigned long sweep_time = 0;
static bool sweep_decrease = false;
static int sweep_shift = false;

static int curr_duty_step = 0;

void sound1_init(unsigned long cycles) {
	last_update_length = cycles;
	last_update_duty = cycles;
	last_update_envelope = cycles;
	last_update_sweep = cycles;
}

static inline unsigned long read_shadow_freq(void) {
	return ((mem_read8(NR14_ADDRESS) & 0x07u) << 8u) | mem_read8(NR13_ADDRESS);
}

static inline void write_shadow_freq(unsigned long freq) {
	shadow_freq = freq;
	mem_write8(NR13_ADDRESS, (uint8_t)(freq & 0xff));
	mem_write8(NR14_ADDRESS, (mem_read8(NR14_ADDRESS) & 0xc0) | ((uint8_t)(freq >> 8) & 0x07));

	if (shadow_freq == 0 || shadow_freq > 0x7ff) {
		enabled = false;
		mem_write8(NR52_ADDRESS, mem_read8(NR52_ADDRESS) & 0xfe);
	} else {
		selected_period = SHADOW2SELECTED(shadow_freq);
	}
}

static inline void sweep_step(bool update) {
	unsigned long newfreq;
	unsigned long delta = shadow_freq >> sweep_shift;
	if (sweep_decrease)
		newfreq = shadow_freq - delta;
	else
		newfreq = shadow_freq + delta;

	if (newfreq > 0x7ff || shadow_freq < delta) {
		enabled = false;
		mem_write8(NR52_ADDRESS, mem_read8(NR52_ADDRESS) & 0xfe);
	} else if (update && sweep_shift != 0) {
		write_shadow_freq(newfreq);
	}
}

void sound1_update(unsigned long cycles) {
	enabled = (mem_read8(NR52_ADDRESS) & 0x01) != 0;
	uint8_t NR11 = mem_read8(NR11_ADDRESS);
	uint8_t NR12 = mem_read8(NR12_ADDRESS);
	if (mem_read8(NR14_ADDRESS) & 0x80) {
		// reset
		enabled = true;
		// reset init flag
		mem_write8(NR14_ADDRESS, mem_read8(NR14_ADDRESS) & 0x7f);
		// enable sound2
		mem_write8(NR52_ADDRESS, mem_read8(NR52_ADDRESS) | 0x01);

		curr_volume = (NR12 >> 4) & 0x0f;
		curr_duty_step = 0;
	}
	selected_duty = ((NR11 & 0xc0) >> 6) & 0x03;
	selected_length = 64 - (NR11 & 0x3f);

	// Volume gets updated only on resets except when set to zero
	if (((NR12 >> 4) & 0x0f) == 0) {
		curr_volume = 0;
	}
	envelope_increase = NR12 & 0x08;
	envelope_len = NR12 & 0x07;

	uint8_t NR10 = mem_read8(NR10_ADDRESS);
	sweep_time = (NR10 & NR10_SWEEP_TIME_MASK) >> 4;
	sweep_decrease = NR10 & NR10_SWEEP_DECREASE_MASK;
	sweep_shift = NR10 & NR10_SWEEP_SHIFT_MASK;

	shadow_freq = read_shadow_freq();
	selected_period = SHADOW2SELECTED(shadow_freq);

	if (!enabled) {
		last_update_length = cycles;
		last_update_duty = cycles;
		last_update_envelope = cycles;
		last_update_sweep = cycles;
	}
	bool updated = true;
	while (enabled && updated) {
		updated = false;
		// should be 4_194_304 / 64
		if (sweep_time != 0 && sweep_shift != 0
			&& cycles - last_update_sweep >= CPU_FREQ / (128 / sweep_time)) {
			last_update_sweep += CPU_FREQ / (128 / sweep_time);
			updated = true;

			sweep_step(true);
			sweep_step(false);
		}

		// should be 4_194_304 / 64
		if (envelope_len > 0 && cycles - last_update_envelope >= CPU_FREQ / (64 / envelope_len)) {
			last_update_envelope += CPU_FREQ / (64 / envelope_len);
			updated = true;

			envelope_len--;
			if ((envelope_increase && curr_volume < 0x0f)
				|| (!envelope_increase && curr_volume > 0x00))
				curr_volume += envelope_increase ? 1 : -1;

			if ((envelope_increase && curr_volume == 0x0f)
				|| (!envelope_increase && curr_volume == 0x00))
				envelope_len = 0;
			mem_write8(NR12_ADDRESS,
					   (mem_read8(NR12_ADDRESS) & 0x0f) | ((curr_volume << 4) & 0xf0));
		}

		// should be 4_194_304 / 256 = 16384
		bool is_continuous = !(mem_read8(NR14_ADDRESS) & 0x40);
		if (!is_continuous && cycles - last_update_length >= CPU_FREQ / 256) {
			last_update_length += CPU_FREQ / 256;
			updated = true;

			selected_length--;
			if (selected_length == 0) {
				enabled = false;
				mem_write8(NR52_ADDRESS, mem_read8(NR52_ADDRESS) & 0xfe);
			}
		}

		if (cycles - last_update_duty >= selected_period) {
			last_update_duty += selected_period;
			updated = true;
			curr_duty_step = (curr_duty_step + 1) % 8;
		}
	}
}

uint8_t sound1_get_sample(void) {
	if (!enabled)
		return 0;
	return duty_cycles[selected_duty][curr_duty_step] * curr_volume;
}
