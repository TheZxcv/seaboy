#include "audio/sound4.h"
#include "seaboy_audio.h"
#include "seaboy_mmu.h"

#include <stdbool.h>

static unsigned long last_update_length = 0;
static unsigned long last_update_envelope = 0;
static unsigned long last_update_output = 0;

static bool enabled = false;
static int selected_length = 0;
static uint8_t curr_volume = 0;
static unsigned long envelope_len = 0;
static bool envelope_increase = false;
unsigned long selected_freq = 0;

static uint16_t shift_reg = 0;
static uint8_t output_state = 0;

// f = CPU_FREQ
/*static unsigned long dividers_table[] = {
	4, // f * (1/8) * 2
	8, // f * (1/8)
	16,// f * (1/8) * (1/2)
	24,// f * (1/8) * (1/3)
	32,// f * (1/8) * (1/4)
	40,// f * (1/8) * (1/5)
	48,// f * (1/8) * (1/6)
	56 // f * (1/8) * (1/7)
};*/
/* clang-format off */
static unsigned long dividers_table[] = {
	(CPU_FREQ / 8) * 2,
	(CPU_FREQ / 8),
	(CPU_FREQ / 8) / 2,
	(CPU_FREQ / 8) / 3,
	(CPU_FREQ / 8) / 4,
	(CPU_FREQ / 8) / 5,
	(CPU_FREQ / 8) / 6,
	(CPU_FREQ / 8) / 7,
};
/* clang-format on */

void sound4_init(unsigned long cycles) {
	last_update_length = cycles;
	last_update_envelope = cycles;
	last_update_output = cycles;
}

void sound4_update(unsigned long cycles) {
	enabled = (mem_read8(NR52_ADDRESS) & NR52_SOUND4_ENABLE_MASK) != 0;
	if (mem_read8(NR44_ADDRESS) & 0x80) {
		enabled = true;
		// reset init flag
		mem_write8(NR44_ADDRESS, mem_read8(NR44_ADDRESS) & 0x7f);
		// enable sound4
		mem_write8(NR52_ADDRESS, mem_read8(NR52_ADDRESS) | NR52_SOUND4_ENABLE_MASK);

		last_update_length = cycles;
		last_update_envelope = cycles;
		last_update_output = cycles;
		output_state = 0;
		shift_reg = 0xffff;
	}

	uint8_t NR41 = mem_read8(NR41_ADDRESS);
	selected_length = 64 - (NR41 & 0x3f);

	bool updated = true;
	while (enabled && updated) {
		updated = false;

		// reload envelope data
		uint8_t NR42 = mem_read8(NR42_ADDRESS);
		curr_volume = (NR42 >> 4) & 0x0f;
		envelope_increase = NR42 & 0x08;
		envelope_len = NR42 & 0x07;
		// should be 4_194_304 / 64
		if (envelope_len > 0 && cycles - last_update_envelope >= CPU_FREQ / (64 / envelope_len)) {
			last_update_envelope += CPU_FREQ / (64 / envelope_len);
			updated = true;

			envelope_len--;
			if ((envelope_increase && curr_volume < 0x0f)
				|| (!envelope_increase && curr_volume > 0x00))
				curr_volume += envelope_increase ? 1 : -1;

			if ((envelope_increase && curr_volume == 0x0f)
				|| (!envelope_increase && curr_volume == 0x00))
				envelope_len = 0;
			mem_write8(NR42_ADDRESS,
					   (mem_read8(NR42_ADDRESS) & 0x0f) | ((curr_volume << 4) & 0xf0));
		}

		// should be 4_194_304 / 256 = 16384
		bool is_continuous = !(mem_read8(NR44_ADDRESS) & 0x40);
		if (!is_continuous && cycles - last_update_length >= CPU_FREQ / 256) {
			last_update_length += CPU_FREQ / 256;
			updated = true;

			selected_length--;
			if (selected_length == 0) {
				enabled = false;
				mem_write8(NR52_ADDRESS,
						   mem_read8(NR52_ADDRESS) & ((~NR52_SOUND4_ENABLE_MASK) & 0xff));
			}
		}

		// reload frequency data
		uint8_t NR43 = mem_read8(NR43_ADDRESS);
		unsigned int shift_clock_freq = (NR43 >> 4) & 0x0f;
		bool shift_reg_mode = NR43 & 0x08;
		unsigned int dividing_ratio = NR43 & 0x07;
		selected_freq = dividers_table[dividing_ratio] >> (shift_clock_freq + 1);

		if (cycles - last_update_output >= CPU_FREQ / selected_freq) {
			last_update_output += CPU_FREQ / selected_freq;
			updated = true;

			uint16_t xored = (shift_reg ^ (shift_reg >> 1)) & 0x01;
			shift_reg = (shift_reg >> 1);
			if (shift_reg_mode)
				shift_reg |= (xored << 6);
			else
				shift_reg |= (xored << 14);
			output_state = (shift_reg & 0x01) ? 0 : 1;
		}
	}
}

uint8_t sound4_get_sample(void) {
	return output_state * curr_volume;
}
