#include "audio/sound2.h"
#include "seaboy_audio.h"
#include "seaboy_mmu.h"

#include <stdbool.h>
#include <stddef.h>

static unsigned long last_update_length = 0;
static unsigned long last_update_duty = 0;
static unsigned long last_update_envelope = 0;

static bool enabled = false;
static int selected_length = 0;
static int selected_duty = 0;
static uint8_t curr_volume = 0;
static unsigned long envelope_len = 0;
static bool envelope_increase = false;
static unsigned long selected_freq = 0;

static int curr_duty_step = 0;

void sound2_init(unsigned long cycles) {
	last_update_length = cycles;
	last_update_duty = cycles;
	last_update_envelope = cycles;
}

void sound2_update(unsigned long cycles) {
	enabled = (mem_read8(NR52_ADDRESS) & 0x02) != 0;
	uint8_t NR21 = mem_read8(NR21_ADDRESS);
	uint8_t NR22 = mem_read8(NR22_ADDRESS);
	if (mem_read8(NR24_ADDRESS) & 0x80) {
		// reset
		enabled = true;
		// reset init flag
		mem_write8(NR24_ADDRESS, mem_read8(NR24_ADDRESS) & 0x7f);
		// enable sound2
		mem_write8(NR52_ADDRESS, mem_read8(NR52_ADDRESS) | 0x02);

		curr_volume = (NR22 >> 4) & 0x0f;

		last_update_length = cycles;
		last_update_duty = cycles;
		last_update_envelope = cycles;
	}
	selected_duty = ((NR21 & 0xc0) >> 6) & 0x03;
	selected_length = 64 - (NR21 & 0x3f);

	if (((NR22 >> 4) & 0x0f) == 0) {
		curr_volume = 0;
	}
	envelope_increase = NR22 & 0x08;
	envelope_len = NR22 & 0x07;

	unsigned long raw_freq = ((mem_read8(NR24_ADDRESS) & 0x07) << 8) | mem_read8(NR23_ADDRESS);
	selected_freq = 4 * (2048 - raw_freq);

	bool updated = true;
	while (enabled && updated) {
		updated = false;
		// should be 4_194_304 / 64
		if (envelope_len > 0 && cycles - last_update_envelope >= CPU_FREQ / (64 / envelope_len)) {
			last_update_envelope += CPU_FREQ / (64 / envelope_len);
			updated = true;

			envelope_len--;
			if ((envelope_increase && curr_volume < 0x0f)
				|| (!envelope_increase && curr_volume > 0x00))
				curr_volume += envelope_increase ? 1 : -1;

			if ((envelope_increase && curr_volume == 0x0f)
				|| (!envelope_increase && curr_volume == 0x00))
				envelope_len = 0;
			mem_write8(NR22_ADDRESS,
					   (mem_read8(NR22_ADDRESS) & 0x0f) | ((curr_volume << 4) & 0xf0));
		}

		// should be 4_194_304 / 256 = 16384
		bool is_continuous = !(mem_read8(NR24_ADDRESS) & 0x40);
		if (!is_continuous && cycles - last_update_length >= CPU_FREQ / 256) {
			last_update_length += CPU_FREQ / 256;
			updated = true;

			selected_length--;
			if (selected_length == 0) {
				enabled = false;
				mem_write8(NR52_ADDRESS, mem_read8(NR52_ADDRESS) & 0xfd);
			}
		}

		if (cycles - last_update_duty >= selected_freq) {
			last_update_duty += selected_freq;
			updated = true;
			curr_duty_step = (curr_duty_step + 1) % 8;
		}
	}
}

uint8_t sound2_get_sample(void) {
	if (!enabled)
		return 0;
	return duty_cycles[selected_duty][curr_duty_step] * curr_volume;
}
