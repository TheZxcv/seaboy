#include "audio/sound3.h"
#include "seaboy_audio.h"
#include "seaboy_mmu.h"

#include <stdbool.h>
#include <stdio.h>

static unsigned long last_update_length = 0;
static unsigned long last_update_step = 0;

static bool enabled = false;
static int curr_length;
static unsigned int curr_step;
static uint8_t output_byte;

void sound3_init(unsigned long cycles) {
	last_update_length = cycles;
	last_update_step = cycles;
	curr_step = 0;
	output_byte = 0;
}

static uint8_t get_step_sample(unsigned int step_index) {
	uint8_t step_pair = mem_read8(0xff30 + step_index / 2);
	return (step_index % 2 == 0) ? ((step_pair >> 4) & 0x0f) : (step_pair & 0x0f);
}

void sound3_update(unsigned long cycles) {
	enabled = (mem_read8(NR30_ADDRESS) & 0x80) != 0;
	if (enabled && mem_read8(NR34_ADDRESS) & 0x80) {
		// reset init flag
		mem_write8(NR34_ADDRESS, mem_read8(NR34_ADDRESS) & 0x7f);
		mem_write8(NR52_ADDRESS, mem_read8(NR52_ADDRESS) | 0x04);
		curr_length = 0;
		curr_step = 0;

		last_update_length = cycles;
		last_update_step = cycles;
	}

	enabled = enabled && (mem_read8(NR52_ADDRESS) & 0x04) != 0;

	bool updated = true;
	while (enabled && updated) {
		updated = false;
		int selected_length = 256 - mem_read8(NR31_ADDRESS);

		// should be 4_194_304 / 256 = 16384
		bool is_continuous = !(mem_read8(NR34_ADDRESS) & 0x40);
		if (!is_continuous && cycles - last_update_length >= CPU_FREQ / 256) {
			last_update_length += CPU_FREQ / 256;
			updated = true;

			curr_length++;
			if (curr_length == selected_length) {
				enabled = false;
				mem_write8(NR52_ADDRESS, mem_read8(NR52_ADDRESS) & 0xfb);
				mem_write8(NR30_ADDRESS, mem_read8(NR30_ADDRESS) & 0x7f);
			}
		}

		unsigned long raw_freq = ((mem_read8(NR34_ADDRESS) & 0x07) << 8) | mem_read8(NR33_ADDRESS);
		unsigned long selected_freq = 2 * (2048 - raw_freq);

		if (cycles - last_update_step >= selected_freq) {
			last_update_step += selected_freq;
			updated = true;

			curr_step = (curr_step + 1) % 32;
			int output_level = ((mem_read8(NR32_ADDRESS) & 0x60) >> 5) & 0x3;
			output_byte = get_step_sample(curr_step);
			switch (output_level) {
				case 0: output_byte = 0; break;
				case 2: output_byte >>= 1; break;
				case 3: output_byte >>= 2; break;
				default: break;
			}
		}
	}
}

uint8_t sound3_get_sample(void) {
	return output_byte;
}
