#include "seaboy_video.h"

#include "seaboy_interrupts.h"
#include "seaboy_mmu.h"
#include "utils.h"

#include <assert.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>

#define GET_PIXEL_FROM_TILE_ROW(byteH, byteL, pixel) \
	((((((byteL) >> (7 - pixel)) << 1) & 0x2) | (((byteH) >> (7 - pixel)) & 0x1)) & 0x3)

static unsigned long screen_cycles_per_mode[] = {
	[0] = 205,
	[2] = 80,
	[3] = 171,
	[1] = 456,
};
static unsigned long last_update_screen;
static bool update_diplay;
static bool is_screen_blank;

static uint8_t screen[VIDEORAM_WIDTH * VIDEORAM_HEIGHT];
static uint8_t window[VIDEORAM_WIDTH * VIDEORAM_HEIGHT];
static uint8_t lcd[WIDTH_DOTS * HEIGHT_DOTS];

static void draw_scanline(uint8_t LY);

void init_video_driver(unsigned long starting_cycle) {
	last_update_screen = starting_cycle;
	update_diplay = true;
	memset(lcd, 0, sizeof(lcd));
	is_screen_blank = true;
}

void video_driver(unsigned long cycles) {
	if ((mem_read8(LCDC_ADDRESS) & LCDC_ENABLE_MASK) == 0) {
		// When the LCD is turned off LY is set to 0 and nothing happens (no interrupts)
		// It will restart the whole cycle from the beginning when it's turned back on
		last_update_screen = cycles;
		mem_write8(LY_ADDRESS, 0);
		mem_write8(STAT_ADDRESS, (mem_read8(STAT_ADDRESS) & 0xf8) | 0x00);
		if (!is_screen_blank) {
			memset(lcd, 0, sizeof(lcd));
			is_screen_blank = true;
			update_diplay = true;
		}
		return;
	} else {
		is_screen_blank = false;
	}

	uint8_t stat = mem_read8(STAT_ADDRESS);
	uint8_t mode = stat & STAT_MODE_MASK;
	while (cycles - last_update_screen >= screen_cycles_per_mode[mode]) {
		stat = mem_read8(STAT_ADDRESS);
		mode = stat & STAT_MODE_MASK;
		bool stat_any_already_met = ((stat & STAT_LY_COINCIDENCE_IE_MASK)
									 && (mem_read8(LYC_ADDRESS) == mem_read8(LY_ADDRESS)))
									|| (mode == 2 && (stat & STAT_MODE2_IE_MASK))
									|| (mode == 1 && (stat & STAT_MODE1_IE_MASK))
									|| (mode == 0 && (stat & STAT_MODE0_IE_MASK));
		bool coincidence = false;
		last_update_screen += screen_cycles_per_mode[mode];
		uint8_t LY = mem_read8(LY_ADDRESS);
		switch (mode) {
			case 0:
				if (LY == LY_VBLANK_START_LINE) {
					mode = 1;
					mem_write8(IF_ADDRESS, mem_read8(IF_ADDRESS) | VBLANK_INT);
					update_diplay = true;
				} else {
					mode = 2;
				}
				break;
			case 1:
				LY++;
				if (LY == LY_VBLANK_END_LINE) {
					LY = 0;
					mode = 2;
				}
				mem_write8(LY_ADDRESS, LY);
				break;
			case 2: mode = 3; break;
			case 3:
				draw_scanline(LY);
				LY++;
				mode = 0;
				mem_write8(LY_ADDRESS, LY);
				break;
			default:
				/* unreachable */
				die();
				break;
		}
		coincidence = mem_read8(LYC_ADDRESS) == mem_read8(LY_ADDRESS);

		stat = (mem_read8(STAT_ADDRESS) & 0xf8) | mode;
		if (coincidence)
			stat |= STAT_LY_COINCIDENCE_FLAG_MASK;
		mem_write8(STAT_ADDRESS, stat);

		if (!stat_any_already_met) {
			if ((stat & STAT_LY_COINCIDENCE_IE_MASK) && coincidence) {
				mem_write8(IF_ADDRESS, mem_read8(IF_ADDRESS) | LCD_STAT_INT);
			}

			if ((mode == 2 && (stat & STAT_MODE2_IE_MASK))
				|| (mode == 1 && (stat & STAT_MODE1_IE_MASK))
				|| (mode == 0 && (stat & STAT_MODE0_IE_MASK))) {
				mem_write8(IF_ADDRESS, mem_read8(IF_ADDRESS) | LCD_STAT_INT);
			}
		}
	}
}

bool redraw_display(void) {
	if (update_diplay) {
		update_diplay = false;
		return true;
	}
	return false;
}

static unsigned char get_bg_color(uint8_t palette_index) {
	uint8_t palette = mem_read8(BGP_ADDRESS);
	return (palette >> (palette_index << 1)) & 0x03;
}

static void draw_tile(uint8_t *dst, uint8_t itile, int row, int col) {
	gb_memory_t *mem = get_memory();

	size_t idx;
	// Tile data
	if (mem_read8(LCDC_ADDRESS) & LCDC_BG_TILE_DATA_MASK)
		idx = itile * TILE_SIZE; // 0x8000-0x8fff (0..255)
	else
		// 0x8800-0x97ff (-128..127)
		idx = 0x800 + (BYTE_SIGN_EXT(itile) + 128) * TILE_SIZE;

	row = TILE_WIDTH * row;
	col = TILE_HEIGHT * col;
	for (size_t i = 0; i < 2 * TILE_HEIGHT; i += 2) {
		uint8_t byteH = mem->ramVideo[idx + i];
		uint8_t byteL = mem->ramVideo[idx + i + 1];
		int cc = col;
		for (int8_t ipixel = 0; ipixel < 8; ipixel++) {
			uint8_t palette_index = GET_PIXEL_FROM_TILE_ROW(byteH, byteL, ipixel);
			dst[row * VIDEORAM_WIDTH + cc] = get_bg_color(palette_index);
			cc++;
		}
		row++;
	}
}

static unsigned char get_obj_color(bool is_obp1, uint8_t palette_index) {
	uint8_t palette = is_obp1 ? mem_read8(OBP1_ADDRESS) : mem_read8(OBP0_ADDRESS);
	return (palette >> (palette_index << 1)) & 0x03;
}

static void draw_obj(uint8_t *dst, uint8_t itile, uint8_t row, uint8_t col, uint8_t attr,
					 int line) {
	gb_memory_t *mem = get_memory();

	size_t idx = itile * TILE_SIZE; // 0x8000-0x8fff (0..255)

	uint8_t height = TILE_HEIGHT;
	if (mem_read8(LCDC_ADDRESS) & LCDC_OBJ_SIZE_MASK) {
		height = 2 * TILE_HEIGHT;
	}

	bool flipH = attr & OBJ_ATTR_HFLIP_MASK;
	bool flipV = attr & OBJ_ATTR_VFLIP_MASK;
	if (flipV)
		row = row + height - 1;
	for (size_t i = 0; i < height * 2; i += 2, row += flipV ? -1 : 1) {
		uint8_t byteH = mem->ramVideo[idx + i];
		uint8_t byteL = mem->ramVideo[idx + i + 1];
		uint8_t cc = flipH ? col + TILE_WIDTH - 1 : col;
		for (int8_t ipixel = 0; ipixel < 8; ipixel++, cc += flipH ? -1 : 1) {
			uint8_t palette_index = GET_PIXEL_FROM_TILE_ROW(byteH, byteL, ipixel);
			if (palette_index == 0) // transparent
				continue;

			bool palette_obp1 = attr & OBJ_ATTR_PALETTE_MASK;
			unsigned char color = get_obj_color(palette_obp1, palette_index);
			if (row < HEIGHT_DOTS && cc < WIDTH_DOTS) {
				int ipix = row * WIDTH_DOTS + cc;
				if (!(attr & OBJ_ATTR_PRIORITY_MASK) || dst[ipix] == 0x00) {
					dst[ipix] = color;
				}
			}
		}
	}
}

static void draw_line(uint8_t *dst, const uint8_t *const tilemap, size_t screen_line) {
	int tile_line = (screen_line / TILE_HEIGHT) % TILE_MAP_ROWS;

	const uint8_t *line = &tilemap[TILE_MAP_COLS * tile_line];
	for (int col = 0; col < TILE_MAP_COLS; col++)
		draw_tile(dst, line[col], tile_line, col);
}

static size_t get_tilemap_offset(bool is_alternative) {
	if (is_alternative)
		return 0x9C00 - RAM_VIDEO_OFFSET; // 0x9c00-0x9fff
	else
		return 0x9800 - RAM_VIDEO_OFFSET; // 0x9800-0x9bff
}

struct oam_object {
	uint16_t address;
	uint8_t chr_code;
	uint8_t x;
	uint8_t y;
	uint8_t attr;
};

int compare_objects(const void *a, const void *b) {
	const struct oam_object *obj1 = (const struct oam_object *) a;
	const struct oam_object *obj2 = (const struct oam_object *) b;
	if (obj1->x == obj2->x)
		return obj1->address - obj2->address;
	else
		return obj1->x - obj2->x;
}

static void draw_scanline(uint8_t LY) {
	if (LY >= HEIGHT_DOTS) {
		return;
	}
	gb_memory_t *mem = get_memory();

	// background
	uint8_t scx = mem_read8(SCX_ADDRESS);
	uint8_t scy = mem_read8(SCY_ADDRESS);
	size_t vram_line = LY + scy;
	size_t tilemap_offset = get_tilemap_offset(mem_read8(LCDC_ADDRESS) & LCDC_BG_TILE_MAP_MASK);
	draw_line(screen, &mem->ramVideo[tilemap_offset], vram_line);

	uint8_t wx = mem_read8(WX_ADDRESS);
	uint8_t wy = mem_read8(WY_ADDRESS);
	size_t win_line = LY - wy;
	// windowing
	if ((mem_read8(LCDC_ADDRESS) & LCDC_WINDOW_ENABLE_MASK) && LY >= wy) {
		tilemap_offset = get_tilemap_offset(mem_read8(LCDC_ADDRESS) & LCDC_WINDOW_TILE_MAP_MASK);
		draw_line(window, &mem->ramVideo[tilemap_offset], win_line);
	}

	vram_line %= VIDEORAM_HEIGHT;
	win_line %= VIDEORAM_HEIGHT;
	for (int i = 0; i < WIDTH_DOTS; i++) {
		lcd[LY * WIDTH_DOTS + i] = screen[vram_line * VIDEORAM_WIDTH + (scx + i) % VIDEORAM_WIDTH];
		if ((mem_read8(LCDC_ADDRESS) & LCDC_WINDOW_ENABLE_MASK) && LY >= wy && i + 7 >= wx) {
			lcd[LY * WIDTH_DOTS + i] =
					window[win_line * VIDEORAM_WIDTH + (i - wx + 7) % VIDEORAM_WIDTH];
		}
	}

	// objects
	static struct oam_object objects[(OAM_END_ADDRESS - OAM_START_ADDRESS) / 4 + 1];
	if (mem_read8(LCDC_ADDRESS) & LCDC_OBJ_ENABLE_MASK) {
		for (uint16_t addr = OAM_END_ADDRESS; addr >= OAM_START_ADDRESS; addr -= OBJ_SIZE) {
			int i = (addr - OAM_START_ADDRESS) / 4;
			objects[i].address = addr;
			objects[i].chr_code = mem_read8(addr + 2);
			objects[i].x = mem_read8(addr + 1) - OBJ_X_OFFSET;
			objects[i].y = mem_read8(addr) - OBJ_Y_OFFSET;
			objects[i].attr = mem_read8(addr + 3);
		}

		qsort(objects, 40, sizeof(struct oam_object), compare_objects);

		for (int i = 39; i >= 0; i--) {
			uint8_t height = TILE_HEIGHT;
			if (mem_read8(LCDC_ADDRESS) & LCDC_OBJ_SIZE_MASK) {
				height = 2 * TILE_HEIGHT;
			}
			uint8_t bottom = (objects[i].y + height) % 256;
			bool wraps_around = objects[i].y > bottom;
			if ((LY >= objects[i].y || wraps_around) && LY < bottom) {
				draw_obj(lcd, objects[i].chr_code, objects[i].y, objects[i].x, objects[i].attr,
						 LY - objects[i].y);
			}
		}
	}
}

void get_vram(uint8_t **output) {
	*output = screen;
}

void get_screen(uint8_t **output) {
	*output = lcd;
}
