#include "seaboy_rom.h"
#include "seaboy_mmu.h"
#include "utils.h"

#include <stdio.h>
#include <string.h>

#define MAX_ROM_SIZE (2097152U)
#define MAX_RAM_SIZE (16U * RAM_SWITCHBANK_SIZE)

static ROM_t _rom;
static uint8_t rom_buffer[MAX_ROM_SIZE];
static uint8_t ram_buffer[MAX_RAM_SIZE];
static char *savegame_file = NULL;

bool read_rom_header(const char *filename, rom_header_t *header) {
	FILE *fd = fopen(filename, "rb");
	if (!fd)
		goto fail;

	if (fseek(fd, HEADER_OFFSET, SEEK_SET) < 0)
		goto fail;

	if (fread(header, sizeof(rom_header_t), 1, fd) < 1)
		goto fail;

	return true;

fail:
	if (fd)
		fclose(fd);
	return false;
}

static void print_summary(const rom_header_t *const header, const ROM_t *const rom) {
	printf("%.*s\n", 15, header->title);
	const char *mbc;
	switch (rom->mbc) {
		case NO_MBC: mbc = "ROM ONLY"; break;
		case MBC1: mbc = "MBC1"; break;
		case MBC2: mbc = "MBC2"; break;
		case MBC3: mbc = "MBC3"; break;
		case MBC5: mbc = "MBC5"; break;
		case MBC6: mbc = "MBC6"; break;
		default: mbc = "UNKNOWN"; break;
	}

	printf("[%s", mbc);
	if (rom->has_sram)
		printf("+RAM");
	if (rom->has_battery)
		printf("+BATTERY");
	if (rom->has_rtc)
		printf("+RTC");
	if (rom->has_rumble)
		printf("+RUMBLE");
	printf("]\n");

	size_t rom_size = (32U * 1024U) << header->rom_size;
	printf("ROM: %lu KiB / %d banks\n", rom_size, rom->rom_banks);
	printf("RAM: %lu KiB / %d banks\n", (unsigned long) rom->ram_banks * RAM_SWITCHBANK_SIZE,
		   rom->ram_banks);
}

static void reset_rom(void) {
	_rom.mbc = NO_MBC;
	_rom.has_rtc = false;
	_rom.has_rumble = false;
	_rom.has_sram = false;
	_rom.has_battery = false;

	_rom.rom_banks = 0;
	_rom.ram_banks = 0;

	_rom.rom = NULL;
	_rom.ram = NULL;
}

bool load_savegame(const char *filename, ROM_t *rom) {
	FILE *fd = fopen(filename, "rb");
	if (!fd)
		goto fail;
	size_t savegame_size = rom->ram_banks * RAM_SWITCHBANK_SIZE;
	size_t bytes_read = fread(rom->ram, sizeof(uint8_t), savegame_size, fd);
	if (bytes_read != savegame_size) {
		fprintf(stderr, "Error: malformed savegame.");
		goto fail;
	}

	fclose(fd);
	return true;

fail:
	if (fd)
		fclose(fd);
	return false;
}

bool save_savegame(const char *filename, ROM_t *rom) {
	FILE *fd = fopen(filename, "wb");
	if (!fd)
		goto fail;
	size_t savegame_size = rom->ram_banks * RAM_SWITCHBANK_SIZE;
	size_t bytes_written = fwrite(rom->ram, sizeof(uint8_t), savegame_size, fd);
	if (bytes_written != savegame_size) {
		fprintf(stderr, "Error: failed to save game.");
		goto fail;
	}

	fclose(fd);
	return true;

fail:
	if (fd)
		fclose(fd);
	return false;
}

static char *make_savegame_filename(const char *filename) {
	char *savegame;
	size_t common_part_len = strlen(filename);
	const char *extension = strrchr(filename, '.');
	const char *last_backslash = strrchr(filename, '/');
	const char *last_slash = strrchr(filename, '\\');
	if (extension != NULL && (last_backslash == NULL || extension > last_backslash)
		&& (last_slash == NULL || extension > last_slash)) {
		common_part_len = extension - filename;
	} else {
		common_part_len = strlen(filename);
	}
	savegame = malloc(common_part_len + 5);
	strncpy(savegame, filename, common_part_len);
	savegame[common_part_len] = '.';
	savegame[common_part_len + 1] = 's';
	savegame[common_part_len + 2] = 'a';
	savegame[common_part_len + 3] = 'v';
	savegame[common_part_len + 4] = '\0';
	return savegame;
}

static void _savegame(void) {
	if (savegame_file != NULL) {
		save_savegame(savegame_file, &_rom);
		free(savegame_file);
	}
}

ROM_t *read_rom(const char *filename) {
	FILE *fd = NULL;
	rom_header_t header;
	if (!read_rom_header(filename, &header))
		goto fail;

	reset_rom();

	switch (header.rom_size) {
		case 0x00: _rom.rom_banks = 0; break;
		case 0x01: _rom.rom_banks = 4; break;
		case 0x02: _rom.rom_banks = 8; break;
		case 0x03: _rom.rom_banks = 16; break;
		case 0x04: _rom.rom_banks = 32; break;
		case 0x05: _rom.rom_banks = 64; break;
		case 0x06: _rom.rom_banks = 128; break;
		case 0x07: _rom.rom_banks = 256; break;
		case 0x08: _rom.rom_banks = 512; break;

		case 0x52: _rom.rom_banks = 72; break;
		case 0x53: _rom.rom_banks = 80; break;
		case 0x54: _rom.rom_banks = 96; break;

		default:
			fprintf(stderr, "Unknown ROM size.\n");
			die();
			break;
	}

	switch (header.ram_size) {
		case 0x00: _rom.ram_banks = 0; break;
		case 0x01: _rom.ram_banks = 1; break;
		case 0x02: _rom.ram_banks = 1; break;
		case 0x03: _rom.ram_banks = 4; break;
		case 0x04: _rom.ram_banks = 16; break;
		case 0x05: _rom.ram_banks = 8; break;

		default:
			fprintf(stderr, "Unknown RAM size.\n");
			die();
			break;
	}

	switch (header.type) {
		case 0x00: _rom.mbc = NO_MBC; break;

		case 0x01: _rom.mbc = MBC1; break;
		case 0x02:
			_rom.has_sram = true;
			_rom.mbc = MBC1;
			break;
		case 0x03:
			_rom.has_sram = true;
			_rom.has_battery = true;
			_rom.mbc = MBC1;
			break;

		case 0x05: _rom.mbc = MBC2; break;
		case 0x06:
			_rom.has_battery = true;
			_rom.mbc = MBC2;
			break;

		case 0x08:
			_rom.has_sram = true;
			_rom.mbc = NO_MBC;
			break;
		case 0x09:
			_rom.has_sram = true;
			_rom.has_battery = true;
			_rom.mbc = NO_MBC;
			break;

		case 0x0f:
			_rom.has_rtc = true;
			_rom.has_battery = true;
			_rom.mbc = MBC3;
			break;
		case 0x10:
			_rom.has_sram = true;
			_rom.has_rtc = true;
			_rom.has_battery = true;
			_rom.mbc = MBC3;
			break;

		case 0x11: _rom.mbc = MBC3; break;
		case 0x12:
			_rom.has_sram = true;
			_rom.mbc = MBC3;
			break;
		case 0x13:
			_rom.has_sram = true;
			_rom.has_battery = true;
			_rom.mbc = MBC3;
			break;

		case 0x19: _rom.mbc = MBC5; break;
		case 0x1a:
			_rom.has_sram = true;
			_rom.mbc = MBC5;
			break;
		case 0x1b:
			_rom.has_sram = true;
			_rom.has_battery = true;
			_rom.mbc = MBC5;
			break;

		case 0x1c: _rom.has_rumble = true; break;
		case 0x1d:
			_rom.has_rumble = true;
			_rom.has_sram = true;
			break;
		case 0x1e:
			_rom.has_rumble = true;
			_rom.has_sram = true;
			_rom.has_battery = true;
			_rom.mbc = MBC5;
			break;

		case 0x20: _rom.mbc = MBC6; break;

		default:
			fprintf(stderr, "Unknown cartridge type.\n");
			die();
			break;
	}

	size_t rom_size = (32U * 1024U) << header.rom_size;
	if (rom_size > MAX_ROM_SIZE) {
		fprintf(stderr, "Error: ROM too big %zu bytes.\n", rom_size);
		goto fail;
	}
	fd = fopen(filename, "rb");
	if (!fd)
		goto fail;
	size_t bytes_read = fread(rom_buffer, sizeof(uint8_t), rom_size, fd);

	if (bytes_read != rom_size) {
		fprintf(stderr, "Failure while reading ROM: read %lu, expected %lu.\n", bytes_read,
				rom_size);
		goto fail;
	}

	fclose(fd);

	print_summary(&header, &_rom);

	_rom.rom = rom_buffer;
	_rom.ram = ram_buffer;

	if (_rom.has_battery) {
		savegame_file = make_savegame_filename(filename);
		load_savegame(savegame_file, &_rom);
		atexit(_savegame);
	}

	return &_rom;

fail:
	if (fd)
		fclose(fd);
	return NULL;
}
