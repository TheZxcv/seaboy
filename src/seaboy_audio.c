#include "seaboy_audio.h"
#include "audio/sound1.h"
#include "audio/sound2.h"
#include "audio/sound3.h"
#include "audio/sound4.h"
#include "seaboy_mmu.h"

uint8_t duty_cycles[4][8] = {
	[0] = { 0, 0, 0, 0, 0, 0, 0, 1 }, // 12.5%
	[1] = { 1, 0, 0, 0, 0, 0, 0, 1 }, // 25%
	[2] = { 1, 0, 0, 0, 0, 1, 1, 1 }, // 50%
	[3] = { 0, 1, 1, 1, 1, 1, 1, 0 }, // 75%
};

static play_sample_callback_t play_sample = NULL;

void set_audio_callback(play_sample_callback_t callback) {
	play_sample = callback;
}

void init_audio(unsigned long cycles) {
	sound1_init(cycles);
	sound2_init(cycles);
	sound3_init(cycles);
	sound4_init(cycles);
}

static int16_t samples[SAMPLE_BUFFER_SIZE];
static unsigned long last_sample = 0;
static size_t isample = 0;
#define CHANGE_RANGE(x) ((x) * (32767 / 0x12c) - 16384)

void audio_update(unsigned long cycles) {
	uint8_t NR52 = mem_read8(NR52_ADDRESS);
	uint8_t NR50 = mem_read8(NR50_ADDRESS);

	if (NR52 & NR52_ENABLE_ALL_MASK) {
		sound1_update(cycles);
		sound2_update(cycles);
		sound3_update(cycles);
		sound4_update(cycles);
	}

	// should be 4_194_304 / 44100 = ~95.109
	if (cycles - last_sample >= CPU_FREQ / SAMPLE_RATE) {
		last_sample += CPU_FREQ / SAMPLE_RATE;

		samples[isample] = 0;
		samples[isample + 1] = 0;
		if (NR52 & NR52_ENABLE_ALL_MASK) {
			uint8_t NR51 = mem_read8(NR51_ADDRESS);
			if (NR51 & (NR51_SOUND1_TO_SO1_MASK | NR51_SOUND1_TO_SO2_MASK)) {
				uint8_t output = sound1_get_sample();
				if (NR51 & NR51_SOUND1_TO_SO1_MASK)
					samples[isample] += output;
				if (NR51 & NR51_SOUND1_TO_SO2_MASK)
					samples[isample + 1] += output;
			}

			if (NR51 & (NR51_SOUND2_TO_SO1_MASK | NR51_SOUND2_TO_SO2_MASK)) {
				uint8_t output = sound2_get_sample();
				if (NR51 & NR51_SOUND2_TO_SO1_MASK)
					samples[isample] += output;
				if (NR51 & NR51_SOUND2_TO_SO2_MASK)
					samples[isample + 1] += output;
			}

			if (NR51 & (NR51_SOUND3_TO_SO1_MASK | NR51_SOUND3_TO_SO2_MASK)) {
				uint8_t output = sound3_get_sample();
				if (NR51 & NR51_SOUND3_TO_SO1_MASK)
					samples[isample] += output;
				if (NR51 & NR51_SOUND3_TO_SO2_MASK)
					samples[isample + 1] += output;
			}

			if (NR51 & (NR51_SOUND4_TO_SO1_MASK | NR51_SOUND4_TO_SO2_MASK)) {
				uint8_t output = sound4_get_sample();
				if (NR51 & NR51_SOUND4_TO_SO1_MASK)
					samples[isample] += output;
				if (NR51 & NR51_SOUND4_TO_SO2_MASK)
					samples[isample + 1] += output;
			}

			int16_t main_output_level_SO1 = NR50 & NR50_SO1_LEVEL_MASK;
			samples[isample] = samples[isample] + (samples[isample] * main_output_level_SO1);
			samples[isample] = CHANGE_RANGE(samples[isample]);

			int16_t main_output_level_SO2 = (NR50 & NR50_SO2_LEVEL_MASK) >> 4;
			samples[isample + 1] =
					samples[isample + 1] + (samples[isample + 1] * main_output_level_SO2);
			samples[isample + 1] = CHANGE_RANGE(samples[isample + 1]);

			isample += 2;

			if (isample == SAMPLE_BUFFER_SIZE) {
				isample = 0;
				if (play_sample) {
					play_sample(samples, FRAMES_PER_BUFFER);
				}
			}
		}
	}
}
