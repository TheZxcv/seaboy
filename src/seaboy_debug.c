#include "seaboy_debug.h"

#include <stdio.h>
#include <string.h>

/**
 * LEGENDA:
 *
 * R = reg16
 * r = reg16 alt
 * A = addr16
 * I = imm16
 * i = imm8
 * n = number >> 3
 *
 * f = condition
 * o = ALU operation
 *
 * d = operand/dst
 * s = operand/src
 **/

typedef struct opcode_info_s {
	uint8_t code;
	uint8_t mask;
	char *mnemonic;
} opcode_info_t;

static const opcode_info_t opcode_info[] = {
	{ 0x00, 0xff, "NOP" },
	{ 0x08, 0xff, "LD (%A), SP" },
	{ 0x07, 0xff, "RLC A" },
	{ 0x0f, 0xff, "RRC A" },
	{ 0x17, 0xff, "RL A" },
	{ 0x1f, 0xff, "RR A" },
	{ 0x10, 0xff, "STOP" },
	{ 0x18, 0xff, "JR %i" },
	{ 0x22, 0xff, "LD (HL+), A" },
	{ 0x2a, 0xff, "LD A, (HL+)" },
	{ 0x32, 0xff, "LD (HL-), A" },
	{ 0x3a, 0xff, "LD A, (HL-)" },
	{ 0x27, 0xff, "DAA" },
	{ 0x2f, 0xff, "CPL" },
	{ 0x37, 0xff, "SCF" },
	{ 0x3f, 0xff, "CCF" },
	{ 0x76, 0xff, "HALT" },
	{ 0xc9, 0xff, "RET" },
	{ 0xd9, 0xff, "RETI" },
	{ 0xcd, 0xff, "CALL %A" },
	{ 0xe8, 0xff, "ADD SP, %i" },
	{ 0xf8, 0xff, "LD HL, SP+%i" },
	{ 0xe0, 0xff, "LD (0xff00 + %i), A" },
	{ 0xf0, 0xff, "LD A, (0xff00 + %i)" },
	{ 0xe2, 0xff, "LD (C), A" },
	{ 0xf2, 0xff, "LD A, (C)" },
	{ 0xea, 0xff, "LD (%A), A" },
	{ 0xfa, 0xff, "LD A, (%A)" },
	{ 0xe9, 0xff, "JP HL" },
	{ 0xf9, 0xff, "LD SP, HL" },
	{ 0xf3, 0xff, "DI" },
	{ 0xfb, 0xff, "EI" },
	{ 0xc3, 0xff, "JP %A" },
	{ 0xcb, 0xff, "" }, // cb prefix
	{ 0x02, 0xef, "LD (%R), A" },
	{ 0x0a, 0xef, "LD A, (%R)" },
	{ 0x20, 0xe7, "JR %f, %i" },
	{ 0xc0, 0xe7, "RET %f" },
	{ 0xc2, 0xe7, "JP %f, %A" },
	{ 0xc4, 0xe7, "CALL %f, %A" },
	{ 0x01, 0xcf, "LD %R, %I" },
	{ 0x09, 0xcf, "ADD HL, %R" },
	{ 0x03, 0xcf, "INC %R" },
	{ 0x0b, 0xcf, "DEC %R" },
	{ 0xc1, 0xcf, "POP %r" },
	{ 0xc5, 0xcf, "PUSH %r" },
	{ 0x04, 0xc7, "INC %d" },
	{ 0x05, 0xc7, "DEC %d" },
	{ 0x06, 0xc7, "LD %d, %i" },
	{ 0xc6, 0xc7, "%o A, %i" },
	{ 0xc7, 0xc7, "RST %n" },
	{ 0x40, 0xc0, "LD %d, %s" },
	{ 0x80, 0xc0, "%o A, %s" },
};
static size_t opcode_info_len = sizeof(opcode_info) / sizeof(opcode_info_t);

/* clang-format off */
static const opcode_info_t cb_info[] = {
    { 0x00, 0xf8, "RLC %s" },
    { 0x08, 0xf8, "RRC %s" },
    { 0x10, 0xf8, "RL %s" },
    { 0x18, 0xf8, "RR %s" },
    { 0x20, 0xf8, "SLA %s" },
    { 0x28, 0xf8, "SRA %s" },
    { 0x30, 0xf8, "SWAP %s" },
    { 0x38, 0xf8, "SRL %s" },
    { 0x40, 0xc0, "BIT %n, %s" },
    { 0x80, 0xc0, "RES %n, %s" },
    { 0xc0, 0xc0, "SET %n, %s" },
};
/* clang-format on */
static size_t cb_info_len = sizeof(cb_info) / sizeof(opcode_info_t);

static const opcode_info_t *find_info(const opcode_info_t *infos, size_t len, uint8_t op) {
	for (size_t i = 0; i < len; i++) {
		const opcode_info_t *info = &infos[i];
		if ((op & info->mask) == info->code)
			return info;
	}
	return NULL;
}

static char *reg16[] = {
	"BC",
	"DE",
	"HL",
	"SP",
};

static char *alt_reg16[] = { "BC", "DE", "HL", "AF" };

static char *condition[] = { "NZ", "Z", "NC", "C" };

static char *ALUop[] = { "ADD", "ADC", "SUB", "SBC", "AND", "XOR", "OR", "CP" };

static char *operand[] = { "B", "C", "D", "E", "H", "L", "(HL)", "A" };

static size_t fill_placeholder(char kind, const uint8_t *opcodes, char *buffer, size_t buflen) {
	switch (kind) {
		case 'R': // reg16
			return snprintf(buffer, buflen, "%s", reg16[(*opcodes >> 4) & 0x03]);

		case 'r': // alternative reg16
			return snprintf(buffer, buflen, "%s", alt_reg16[(*opcodes >> 4) & 0x03]);

		case 'A': // address 16bit
			return snprintf(buffer, buflen, "0x%04x", opcodes[1] | (opcodes[2] << 8));

		case 'I': // immediate 16bit
			return snprintf(buffer, buflen, "0x%04x", opcodes[1] | (opcodes[2] << 8));

		case 'i': // immediate 8bit
			return snprintf(buffer, buflen, "0x%02x", opcodes[1]);

		case 'n': // number
			return snprintf(buffer, buflen, "0x%02x", (*opcodes >> 3) & 0x07);

		case 'f': // condition
			return snprintf(buffer, buflen, "%s", condition[(*opcodes >> 3) & 0x03]);

		case 'o': // ALU operation
			return snprintf(buffer, buflen, "%s", ALUop[(*opcodes >> 3) & 0x07]);

		case 'd': // destination
			return snprintf(buffer, buflen, "%s", operand[(*opcodes >> 3) & 0x07]);

		case 's': // source
			return snprintf(buffer, buflen, "%s", operand[(*opcodes) & 0x07]);
		default: return 0;
	}
}

static void format_instr(const char *pattern, const uint8_t *opcodes, char *buffer, size_t buflen) {
	size_t len = strlen(pattern);
	size_t j = 0;
	for (size_t i = 0; i < len && j < buflen - 1; i++) {
		if (pattern[i] != '%') {
			buffer[j++] = pattern[i];
		} else {
			i++;
			size_t spaceLeft = buflen - j; // including the null-terminator
			size_t written = fill_placeholder(pattern[i], opcodes, &buffer[j], spaceLeft);

			if (written == 0) {
				// Nothing has been written which means the format character wasn't valid.
				// Just output the character.
				buffer[j++] = pattern[i];
			} else if (written < spaceLeft - 1) {
				// No worries, there is still space left in the buffer
				j += written;
			} else {
				// The function claims to have written more characters
				// than the space available in the buffer, which means
				// the buffer has been filled!
				j = buflen - 1;
			}
		}
	}
	buffer[j] = '\0';
}

/*
 * Assumes that there are enough bytes to fully decode the instruction!
 */
bool disass(const uint8_t *opcodes, char *buffer, size_t buflen) {
	const opcode_info_t *p = find_info(opcode_info, opcode_info_len, *opcodes);
	if (!p) {
		/* unknown opcode */
		return false;
	}

	/* cb prefix */
	if (strlen(p->mnemonic) == 0) {
		opcodes++; // skipping prefix
		p = find_info(cb_info, cb_info_len, *opcodes);
	}
	if (!p) {
		/* unknown opcode */
		return false;
	}

	format_instr(p->mnemonic, opcodes, buffer, buflen);
	return true;
}
