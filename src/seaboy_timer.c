#include "seaboy_timer.h"

#include "seaboy_interrupts.h"
#include "seaboy_mmu.h"

#include <stdbool.h>
#include <stdint.h>

static unsigned long clock_cycles[] = {
	1024, // f / 2^10
	16,   // f / 2^4
	64,   // f / 2^6
	256,  // f / 2^8
};

static inline bool is_timer_enabled(void) {
	return mem_read8(TAC_ADDRESS) & TIMER_ENABLED_MASK;
}

static inline int clock_selector(void) {
	return mem_read8(TAC_ADDRESS) & TIMER_CLOCK_SELECTOR_MASK;
}

static inline void reset_timer(void) {
	mem_write8(TIMA_ADDRESS, mem_read8(TMA_ADDRESS));
}

static inline void set_timer_interrupt(void) {
	mem_write8(IF_ADDRESS, mem_read8(IF_ADDRESS) | TIMER_INT);
}

static unsigned long last_cycle = 0;
static unsigned long last_cycle_div = 0;

void init_timer(unsigned long starting_cycle) {
	last_cycle = starting_cycle;
}

void timer(unsigned long cycles) {
	if (is_timer_enabled() && cycles > last_cycle) {
		int idx = clock_selector();
		while (cycles - last_cycle >= clock_cycles[idx]) {
			last_cycle += clock_cycles[idx];

			uint8_t tima = mem_read8(TIMA_ADDRESS);
			// is it about to overflow?
			if (tima == 0xff) {
				reset_timer();
				set_timer_interrupt();
			} else {
				// increase timer
				mem_write8(TIMA_ADDRESS, tima + 1);
			}
		}
	} else {
		last_cycle = cycles;
	}

	if (cycles > last_cycle_div) {
		while (cycles - last_cycle_div >= 512) {
			last_cycle_div += 512;

			uint8_t div = mem_read8(DIV_ADDRESS);
			if (div == 0xff) {
				mem_write8(DIV_ADDRESS, 0);
			} else {
				mem_write8(DIV_ADDRESS, div + 1);
			}
		}
	} else {
		last_cycle_div = cycles;
	}
}
