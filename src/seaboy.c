#include "seaboy.h"

#include "seaboy_audio.h"
#include "seaboy_core.h"
#include "seaboy_debug.h"
#include "seaboy_interrupts.h"
#include "seaboy_mmu.h"
#include "seaboy_rom.h"
#include "seaboy_timer.h"
#include "seaboy_video.h"
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>

static const int bytesPerInstr[256] = {
	[0x00] = 1, [0x01] = 3, [0x02] = 1, [0x03] = 1, [0x04] = 1, [0x05] = 1, [0x06] = 2, [0x07] = 1,
	[0x08] = 3, [0x09] = 1, [0x0a] = 1, [0x0b] = 1, [0x0c] = 1, [0x0d] = 1, [0x0e] = 2, [0x0f] = 1,
	[0x10] = 2, [0x11] = 3, [0x12] = 1, [0x13] = 1, [0x14] = 1, [0x15] = 1, [0x16] = 2, [0x17] = 1,
	[0x18] = 2, [0x19] = 1, [0x1a] = 1, [0x1b] = 1, [0x1c] = 1, [0x1d] = 1, [0x1e] = 2, [0x1f] = 1,
	[0x20] = 2, [0x21] = 3, [0x22] = 1, [0x23] = 1, [0x24] = 1, [0x25] = 1, [0x26] = 2, [0x27] = 1,
	[0x28] = 2, [0x29] = 1, [0x2a] = 1, [0x2b] = 1, [0x2c] = 1, [0x2d] = 1, [0x2e] = 2, [0x2f] = 1,
	[0x30] = 2, [0x31] = 3, [0x32] = 1, [0x33] = 1, [0x34] = 1, [0x35] = 1, [0x36] = 2, [0x37] = 1,
	[0x38] = 2, [0x39] = 1, [0x3a] = 1, [0x3b] = 1, [0x3c] = 1, [0x3d] = 1, [0x3e] = 2, [0x3f] = 1,
	[0x40] = 1, [0x41] = 1, [0x42] = 1, [0x43] = 1, [0x44] = 1, [0x45] = 1, [0x46] = 1, [0x47] = 1,
	[0x48] = 1, [0x49] = 1, [0x4a] = 1, [0x4b] = 1, [0x4c] = 1, [0x4d] = 1, [0x4e] = 1, [0x4f] = 1,
	[0x50] = 1, [0x51] = 1, [0x52] = 1, [0x53] = 1, [0x54] = 1, [0x55] = 1, [0x56] = 1, [0x57] = 1,
	[0x58] = 1, [0x59] = 1, [0x5a] = 1, [0x5b] = 1, [0x5c] = 1, [0x5d] = 1, [0x5e] = 1, [0x5f] = 1,
	[0x60] = 1, [0x61] = 1, [0x62] = 1, [0x63] = 1, [0x64] = 1, [0x65] = 1, [0x66] = 1, [0x67] = 1,
	[0x68] = 1, [0x69] = 1, [0x6a] = 1, [0x6b] = 1, [0x6c] = 1, [0x6d] = 1, [0x6e] = 1, [0x6f] = 1,
	[0x70] = 1, [0x71] = 1, [0x72] = 1, [0x73] = 1, [0x74] = 1, [0x75] = 1, [0x76] = 1, [0x77] = 1,
	[0x78] = 1, [0x79] = 1, [0x7a] = 1, [0x7b] = 1, [0x7c] = 1, [0x7d] = 1, [0x7e] = 1, [0x7f] = 1,
	[0x80] = 1, [0x81] = 1, [0x82] = 1, [0x83] = 1, [0x84] = 1, [0x85] = 1, [0x86] = 1, [0x87] = 1,
	[0x88] = 1, [0x89] = 1, [0x8a] = 1, [0x8b] = 1, [0x8c] = 1, [0x8d] = 1, [0x8e] = 1, [0x8f] = 1,
	[0x90] = 1, [0x91] = 1, [0x92] = 1, [0x93] = 1, [0x94] = 1, [0x95] = 1, [0x96] = 1, [0x97] = 1,
	[0x98] = 1, [0x99] = 1, [0x9a] = 1, [0x9b] = 1, [0x9c] = 1, [0x9d] = 1, [0x9e] = 1, [0x9f] = 1,
	[0xa0] = 1, [0xa1] = 1, [0xa2] = 1, [0xa3] = 1, [0xa4] = 1, [0xa5] = 1, [0xa6] = 1, [0xa7] = 1,
	[0xa8] = 1, [0xa9] = 1, [0xaa] = 1, [0xab] = 1, [0xac] = 1, [0xad] = 1, [0xae] = 1, [0xaf] = 1,
	[0xb0] = 1, [0xb1] = 1, [0xb2] = 1, [0xb3] = 1, [0xb4] = 1, [0xb5] = 1, [0xb6] = 1, [0xb7] = 1,
	[0xb8] = 1, [0xb9] = 1, [0xba] = 1, [0xbb] = 1, [0xbc] = 1, [0xbd] = 1, [0xbe] = 1, [0xbf] = 1,
	[0xc0] = 1, [0xc1] = 1, [0xc2] = 3, [0xc3] = 3, [0xc4] = 3, [0xc5] = 1, [0xc6] = 2, [0xc7] = 1,
	[0xc8] = 1, [0xc9] = 1, [0xca] = 3, [0xcb] = 2, [0xcc] = 3, [0xcd] = 3, [0xce] = 2, [0xcf] = 1,
	[0xd0] = 1, [0xd1] = 1, [0xd2] = 3, [0xd3] = 0, [0xd4] = 3, [0xd5] = 1, [0xd6] = 2, [0xd7] = 1,
	[0xd8] = 1, [0xd9] = 1, [0xda] = 3, [0xdb] = 0, [0xdc] = 3, [0xdd] = 0, [0xde] = 2, [0xdf] = 1,
	[0xe0] = 2, [0xe1] = 1, [0xe2] = 1, [0xe3] = 0, [0xe4] = 0, [0xe5] = 1, [0xe6] = 2, [0xe7] = 1,
	[0xe8] = 2, [0xe9] = 1, [0xea] = 3, [0xeb] = 0, [0xec] = 0, [0xed] = 0, [0xee] = 2, [0xef] = 1,
	[0xf0] = 2, [0xf1] = 1, [0xf2] = 1, [0xf3] = 1, [0xf4] = 0, [0xf5] = 1, [0xf6] = 2, [0xf7] = 1,
	[0xf8] = 2, [0xf9] = 1, [0xfa] = 3, [0xfb] = 1, [0xfc] = 0, [0xfd] = 0, [0xfe] = 2, [0xff] = 1
};

int opcode_len(uint8_t op) {
	return bytesPerInstr[op];
}

static struct seaboy_regs *regs;
static bool screen_updated;
static unsigned long cycles;
static int frameskip = 0;

void print_regs(void) {
	printf("AF: 0x%02X|0x%02X\n", regs->A, regs->F);
	printf("BC: 0x%02X|0x%02X\n", regs->B, regs->C);
	printf("DE: 0x%02X|0x%02X\n", regs->D, regs->E);
	printf("HL: 0x%02X|0x%02X\n", regs->H, regs->L);
	printf("SP:  0x%04X\n", regs->SP);
	printf("PC:  0x%04X\n", regs->PC);
	printf("FLAGS:\n\tZ N H C\n");
	printf("\t%d:%d:%d:%d\n", GETF(FLAG_Z, (*regs)), GETF(FLAG_N, (*regs)), GETF(FLAG_H, (*regs)),
		   GETF(FLAG_C, (*regs)));
	printf("IME: %d\n", IME);
	printf("IE: %02x\n", mem_read8(IE_ADDRESS));
	printf("IF: %02x\n", mem_read8(IF_ADDRESS));
}

void seaboy_init(void) {
	regs = get_regs();
	regs->A = 0x01; // GB
	regs->F = 0xB0;
	regs->B = 0x00;
	regs->C = 0x13;
	regs->D = 0x00;
	regs->E = 0xD8;
	regs->H = 0x01;
	regs->L = 0x4D;
	regs->SP = STACK_TOP_OFFSET;
	regs->PC = ENTRY_POINT;
	SETF(FLAG_C, (*regs));
	CPU_STATE = RUNNING;
	IME = false;
	init_timer(0);
	init_video_driver(0);
	init_audio(0);
	mem_write8(JOYP_ADDRESS, 0xcf);
	mem_write8(LCDC_ADDRESS, 0x91);
	mem_write8(STAT_ADDRESS, 0x00);
	mem_write8(BGP_ADDRESS, 0xe4);
	mem_write8(OBP0_ADDRESS, 0xe4);
	mem_write8(OBP1_ADDRESS, 0xe4);
	screen_updated = true;
	cycles = 0;
}

bool load_rom(const char *filename) {
	ROM_t *rom = read_rom(filename);
	if (rom != NULL) {
		return plug_rom(rom);
	} else {
		return false;
	}
}

static opcode_t fetch() {
	opcode_t op = { 0, 0, 0 };
	op.main = mem_read8(regs->PC++);
	int len = opcode_len(op.main);
	if (len == 0) {
		printf("Illegal opcode at [%04x] => 0x%02x\n", regs->PC, op.main);
		die();
	}

	if (len > 1)
		op.low = mem_read8(regs->PC++);
	if (len > 2)
		op.high = mem_read8(regs->PC++);
	return op;
}

void interrupts_routine(void) {
	static uint16_t interrupt_vectors[] = { VBLANK_VECTOR, LCD_STAT_VECTOR, TIMER_VECTOR,
											SERIAL_VECTOR, JOYPAD_VECTOR };
	uint8_t int_enables = mem_read8(IE_ADDRESS);
	uint8_t int_flags = mem_read8(IF_ADDRESS);

	int int_num;
	for (int_num = 0; int_num < 5; int_num++) {
		uint8_t mask = (uint8_t)(1u << int_num);
		if ((int_enables & mask) && (int_flags & mask)) {
			// clear interrupt flag
			mem_write8(IF_ADDRESS, int_flags ^ mask);
			// wake up CPU (needed to exit HALT state)
			CPU_STATE = RUNNING;
			// nested interrupts are disabled by default
			IME = false;
			// push current PC into the stack
			regs->SP -= 2;
			mem_write16(regs->SP, regs->PC);
			// jump to the interrupt vector
			regs->PC = interrupt_vectors[int_num];
			break;
		}
	}
}

inline void seaboy_step(void) {
	uint16_t oldPC;
	if (CPU_STATE != POWERED_OFF) {
		if (IME) {
			interrupts_routine();
		} else if (CPU_STATE == HALTED) {
			// wake up if an enabled interrupt happened
			// even if interrupts are disabled
			if ((mem_read8(IE_ADDRESS) & mem_read8(IF_ADDRESS)) != 0)
				CPU_STATE = RUNNING;
		}

		int clocks = 1;
		if (CPU_STATE != HALTED) {
			oldPC = regs->PC;
			opcode_t op = fetch();
			instruction_t instr = decode(op);
			clocks = instr(op);
			if (regs->PC == oldPC && !IME) {
				CPU_STATE = POWERED_OFF;
			}
		}

		cycles += clocks;

		video_driver(cycles);
		timer(cycles);
		audio_update(cycles);
	}
}

void seaboy_run(void) {
	seaboy_init();

	while (CPU_STATE != POWERED_OFF) {
		seaboy_step();
	}
}

void seaboy_run_till_vblank(void) {
	if (CPU_STATE == POWERED_OFF)
		return;

	for (int i = 0; i <= frameskip; i++) {
		do {
			seaboy_step();
		} while (CPU_STATE != POWERED_OFF && !redraw_display());
	}
}

void set_frameskip(int fs) {
	frameskip = fs;
}
