#include "seaboy.h"
#include <stdlib.h>

int main(int argc, char *argv[]) {
	if (argc > 1) {
		if (!load_rom(argv[1]))
			return EXIT_FAILURE;
		seaboy_run();
	}
	return EXIT_SUCCESS;
}
