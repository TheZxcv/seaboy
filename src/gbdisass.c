#include "seaboy.h"
#include "seaboy_debug.h"
#include "seaboy_rom.h"

#include <stdio.h>
#include <sys/stat.h>

#define BUFFER_SIZE 256

void print_header(const rom_header_t *header) {
	printf("Title: %.*s\n", (int) sizeof(header->title), header->title);
	printf("Manufacturer: %.*s\n", (int) sizeof(header->manufacturer), header->manufacturer);
	printf("CGB flags: 0x%02x\n", header->cgb_flags);
	printf("Licensee code: %.*s\n", (int) sizeof(header->licensee_code), header->licensee_code);
	printf("SGB flags: 0x%02x\n", header->sgb_flags);
	printf("Cartridge type: 0x%02x\n", header->type);
	printf("ROM size: %d\n", header->rom_size);
	printf("RAM size: %d\n", header->ram_size);
	printf("Japan-only: %d\n", header->japan_only);
	printf("Old licensee code: %d\n", header->licensee_code_old);
	printf("ROM version: %d\n", header->rom_version);
	printf("ROM version: %d\n", header->rom_version);
	printf("Header checksum: 0x%02x\n", header->header_checksum);
	printf("Checksum: 0x%04x\n", header->checksum);
}

int disass_file(const char *filename) {
	rom_header_t header;
	if (read_rom_header(filename, &header)) {
		print_header(&header);
		putchar('\n');
	}

	FILE *fd = fopen(filename, "rb");
	if (!fd)
		return -1;

	struct stat st;
	fstat(fileno(fd), &st);

	off_t filelen = st.st_size;

	uint8_t *blob = malloc(filelen * sizeof(uint8_t));
	fread(blob, sizeof(uint8_t), filelen, fd);
	fclose(fd);

	char buffer[BUFFER_SIZE];
	for (int i = 0; i < filelen;) {
		printf("0x%04x:", i);

		int len = opcode_len(blob[i]);
		if (len == 0) {
			// unknown
			printf(" 0x%02x\n", blob[i++]);
			continue;
		}

		for (int j = 0; j < len; j++)
			printf(" 0x%02x", blob[i + j]);
		for (int j = 4 - len; j > 0; j--)
			printf("     ");
		if (disass(&blob[i], buffer, 256))
			printf("%s\n", buffer);
		i += len;
	}
	free(blob);
	return 0;
}

int main(int argc, char *argv[]) {
	for (int i = 1; i < argc; i++) {
		if (disass_file(argv[i]) < 0)
			return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
