#include "seaboy_mmu.h"

#include "memory/mbc1.h"
#include "memory/mbc3.h"
#include "memory/mbc5.h"
#include "seaboy_interrupts.h"
#include "seaboy_joypad.h"
#include "seaboy_video.h"
#include "utils.h"

#include <assert.h>
#include <memory.h>
#include <stddef.h>
#include <stdio.h>

static gb_memory_t mem = { 0 };
static void (*handle_serial)(uint8_t) = NULL;
static void (*mbc_write8)(uint16_t, uint8_t) = NULL;

gb_memory_t *get_memory(void) {
	return &mem;
}

bool plug_rom(ROM_t *rom) {
	mem.cartridge = rom;
	mem.romBanks = &rom->rom[ROM_BANK0_OFFSET];
	mem.romBank0 = &rom->rom[ROM_BANK0_OFFSET];
	mem.romSwitchBank = &rom->rom[ROM_SWITCHBANK_OFFSET];
	mem.ramSwitchBank = rom->ram;
	mem.ramBanks = rom->ram;

	switch (rom->mbc) {
		case NO_MBC: break;
		case MBC1: mbc_write8 = mbc1_write8; break;
		case MBC2: mbc_write8 = mbc1_write8; break;
		case MBC3: mbc_write8 = mbc3_write8; break;
		case MBC5: mbc_write8 = mbc5_write8; break;
		default: die(); break;
	}
	return true;
}

bool is_rom(uint16_t address) {
	return address < RAM_VIDEO_OFFSET;
}

// used to simulate reads from/writes to unusable areas
static uint8_t zero = 0;

uint16_t resolve_address(uint16_t address, uint8_t **mem_block) {
	zero = 0;
	uint16_t offset;
	if (address < ROM_SWITCHBANK_OFFSET) {
		offset = ROM_BANK0_OFFSET;
		*mem_block = mem.romBank0;
	} else if (address < RAM_VIDEO_OFFSET) {
		offset = ROM_SWITCHBANK_OFFSET;
		*mem_block = mem.romSwitchBank;
	} else if (address < RAM_SWITCHBANK_OFFSET) {
		offset = RAM_VIDEO_OFFSET;
		*mem_block = mem.ramVideo;
	} else if (address < RAM_OFFSET) {
		offset = RAM_SWITCHBANK_OFFSET;
		*mem_block = mem.ramSwitchBank;
	} else if (address < RAM_ECHO_OFFSET) {
		offset = RAM_OFFSET;
		*mem_block = mem.ram;
	} else if (address < RAM_OAM_OFFSET) {
		offset = RAM_ECHO_OFFSET;
		*mem_block = mem.ram;
	} else if (address < RAM_UNUSABLE1_OFFSET) {
		offset = RAM_OAM_OFFSET;
		*mem_block = mem.ramOAM;
	} else if (address < RAM_IOPORTS_OFFSET) {
		// unusable area at offset RAM_UNUSABLE1_OFFSET
		offset = address;
		*mem_block = &zero;
	} else if (address < STACK_BOTTOM_OFFSET) {
		offset = RAM_IOPORTS_OFFSET;
		*mem_block = mem.ioports;
	} else {
		offset = STACK_BOTTOM_OFFSET;
		*mem_block = mem.stack;
	}
	assert(offset <= address);
	return address - offset;
}

static uint8_t *get_pointer(uint16_t address) {
	uint8_t *ptr;
	uint16_t offset = resolve_address(address, &ptr);
	return &ptr[offset];
}

uint8_t mem_read8(uint16_t address) {
	return *get_pointer(address);
}

uint16_t mem_read16(uint16_t address) {
	uint8_t low = *get_pointer(address);
	// TODO: this could fail if the address is 0xffff
	uint8_t high = *get_pointer(address + UINT16_C(1));
	return TO_WORD(high, low);
}

bool mem_write8(uint16_t address, uint8_t value) {
	if (is_rom(address)) {
		if (mbc_write8) {
			mbc_write8(address, value);
		}
		return true;
	}

	if (address == SERIAL_DATA_ADDRESS && handle_serial)
		handle_serial(value);
	else if (address == DMA_ADDRESS) {
		// DMA transfer
		uint16_t base = value << 8;
		for (uint16_t i = 0; i < DMA_TRANSFER_SIZE; i++)
			mem_write8(DMA_BASE_ADDRESS | i, mem_read8(base | i));
	} else if (address == JOYP_ADDRESS) {
		uint8_t old = *get_pointer(address);
		value = query_input_state(value) | 0xf0;
		if (((value & 0x0f) != 0x0f) && ((old & 0x0f) == 0x0f))
			mem_write8(IF_ADDRESS, mem_read8(IF_ADDRESS) | JOYPAD_INT);
	}
	*get_pointer(address) = value;
	return true;
}

bool mem_write16(uint16_t address, uint16_t value) {
	uint8_t low = WORD_LOBYTE(value);
	uint8_t high = WORD_HIBYTE(value);
	mem_write8(address, low);
	// TODO: this could fail if the address is 0xffff
	mem_write8(address + UINT16_C(1), high);
	return true;
}

void set_serial_callback(void (*callback)(uint8_t)) {
	handle_serial = callback;
}
