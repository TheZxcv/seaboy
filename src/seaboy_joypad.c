#include "seaboy_joypad.h"

static uint8_t buttons = 0x0f;
static uint8_t direction = 0x0f;

inline uint8_t get_buttons_state(void) {
	return buttons;
}

inline uint8_t get_direction_state(void) {
	return direction;
}

uint8_t query_input_state(uint8_t query) {
	if ((query & QUERY_RESET) == QUERY_RESET)
		return 0xff;
	else if (query & QUERY_BUTTONS_STATE)
		return QUERY_BUTTONS_STATE | get_buttons_state();
	else if (query & QUERY_DIRECTION_STATE)
		return QUERY_DIRECTION_STATE | get_direction_state();
	return query | 0x0f;
}

void press_button(enum seaboy_button btn) {
	uint8_t mask = (~((uint8_t) btn)) & 0x0f;
	buttons &= mask;
}

void release_button(enum seaboy_button btn) {
	buttons |= btn;
}

void press_direction(enum seaboy_direction dir) {
	uint8_t mask = (~((uint8_t) dir)) & 0x0f;
	direction &= mask;
}

void release_direction(enum seaboy_direction dir) {
	direction |= dir;
}
