#include "seaboy_core.h"

#include "seaboy_mmu.h"
#include "utils.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

enum seaboy_cpustate CPU_STATE = POWERED_OFF;
static struct seaboy_regs regs = { 0 };
bool IME = false;

struct seaboy_regs *get_regs(void) {
	return &regs;
}

enum data_length { BYTE = 8, WORD = 16 };
static void compute_carry(enum data_length bits, uint32_t a, uint32_t b, uint32_t res) {
	COPY_TOF(FLAG_C, regs, ((a ^ b ^ res) & (1 << (bits))));
}

static void half_carry(enum data_length bits, uint32_t a, uint32_t b, uint32_t res) {
	COPY_TOF(FLAG_H, regs, (a ^ b ^ res) & (1 << (bits - 4)));
}

static void compute_borrow(uint32_t a, uint32_t b) {
	COPY_TOF(FLAG_C, regs, (a < b));
}

static void half_borrow(enum data_length bits, uint32_t a, uint32_t b) {
	bool set;
	if (bits == BYTE) {
		set = (int8_t)(a & 0xf) - (int8_t)(b & 0xf) < 0;
	} else {
		set = (int16_t)(a & 0xfff) - (int16_t)(b & 0xfff) < 0;
	}

	COPY_TOF(FLAG_H, regs, set);
}

enum condition { COND_NZ = 0x0, COND_Z = 0x1, COND_NC = 0x2, COND_C = 0x3 };
static bool check_condition(enum condition cond) {
	switch (cond) {
		case COND_NZ: return !GETF(FLAG_Z, regs);
		case COND_Z: return GETF(FLAG_Z, regs);
		case COND_NC: return !GETF(FLAG_C, regs);
		case COND_C: return GETF(FLAG_C, regs);
		default:
			// this should never happen
			return false;
	}
}

enum operand8 {
	REG_B = 0x0,
	REG_C = 0x1,
	REG_D = 0x2,
	REG_E = 0x3,
	REG_H = 0x4,
	REG_L = 0x5,
	MEM_HL = 0x6,
	REG_A = 0x7,
};

enum operand16 { REG_BC = 0x0, REG_DE = 0x1, REG_HL = 0x2, REG_SP = 0x3, REG_AF = 0x4 };

static enum operand8 decode_operand8(int val) {
	assert(val >= 0 && val <= 7);
	return (enum operand8) val;
}

static enum operand16 decode_operand16(int val) {
	assert(val >= 0 && val <= 3);
	return (enum operand16) val;
}

static enum operand16 decode_operand16_alt(int val) {
	enum operand16 op = decode_operand16(val);
	return (op == REG_SP) ? REG_AF : op;
}

static uint8_t get_value8(enum operand8 operand) {
	switch (operand) {
		case REG_B: return regs.B;
		case REG_C: return regs.C;
		case REG_D: return regs.D;
		case REG_E: return regs.E;
		case REG_H: return regs.H;
		case REG_L: return regs.L;
		case MEM_HL: return mem_read8(TO_WORD(regs.H, regs.L));
		case REG_A: return regs.A;
		default:
			// this should never happen
			die();
	}
}

static void set_value8(enum operand8 operand, uint8_t value) {
	switch (operand) {
		case REG_B: regs.B = value; break;
		case REG_C: regs.C = value; break;
		case REG_D: regs.D = value; break;
		case REG_E: regs.E = value; break;
		case REG_H: regs.H = value; break;
		case REG_L: regs.L = value; break;
		case MEM_HL: mem_write8(TO_WORD(regs.H, regs.L), value); break;
		case REG_A: regs.A = value; break;
		default:
			// this should never happen
			die();
	}
}

static uint16_t get_value16(enum operand16 operand) {
	switch (operand) {
		case REG_BC: return TO_WORD(regs.B, regs.C);
		case REG_DE: return TO_WORD(regs.D, regs.E);
		case REG_HL: return TO_WORD(regs.H, regs.L);
		case REG_SP: return regs.SP;
		case REG_AF: regs.F &= 0xf0; return TO_WORD(regs.A, regs.F);
		default:
			// this should never happen
			die();
	}
}

static void set_value16(enum operand16 operand, uint16_t value) {
	switch (operand) {
		case REG_BC: SET_PAIR(regs.B, regs.C, value); break;
		case REG_DE: SET_PAIR(regs.D, regs.E, value); break;
		case REG_HL: SET_PAIR(regs.H, regs.L, value); break;
		case REG_SP: regs.SP = value; break;
		case REG_AF:
			SET_PAIR(regs.A, regs.F, value);
			regs.F &= 0xf0;
			break;
		default:
			// this should never happen
			die();
	}
}

static int NOP(opcode_t opcode) {
	(void) opcode;
	return 4;
}

static int illegal_instr(opcode_t opcode) {
	(void) opcode;
	printf("ILLEGAL INSTRUCTION %04x\n", opcode.main);
	exit(EXIT_FAILURE);
}

static int LD_mem_SP(opcode_t opcode) {
	uint16_t addr = TO_WORD(opcode.high, opcode.low);
	mem_write16(addr, regs.SP);
	return 20;
}

static int RLC_A(opcode_t opcode) {
	(void) opcode;
	uint8_t msb = regs.A & 0x80;
	COPY_TOF(FLAG_C, regs, msb);
	regs.A <<= 1;
	if (msb)
		regs.A |= 0x01;
	regs.A &= 0xff;

	CLEARF(FLAG_Z, regs);
	CLEARF(FLAG_N, regs);
	CLEARF(FLAG_H, regs);
	return 4;
}

static int RRC_A(opcode_t opcode) {
	(void) opcode;
	uint8_t lsb = regs.A & 0x01;
	COPY_TOF(FLAG_C, regs, lsb);
	regs.A >>= 1;
	if (lsb)
		regs.A |= 0x80;

	CLEARF(FLAG_Z, regs);
	CLEARF(FLAG_N, regs);
	CLEARF(FLAG_H, regs);
	return 4;
}

static int RL_A(opcode_t opcode) {
	(void) opcode;
	uint8_t msb = regs.A & 0x80;
	regs.A <<= 1;
	if (GETF(FLAG_C, regs))
		regs.A |= 0x01;
	regs.A &= 0xff;
	COPY_TOF(FLAG_C, regs, msb);

	CLEARF(FLAG_Z, regs);
	CLEARF(FLAG_N, regs);
	CLEARF(FLAG_H, regs);
	return 4;
}

static int RR_A(opcode_t opcode) {
	(void) opcode;
	uint8_t lsb = regs.A & 0x01;
	regs.A >>= 1;
	if (GETF(FLAG_C, regs))
		regs.A |= 0x80;
	COPY_TOF(FLAG_C, regs, lsb);

	CLEARF(FLAG_Z, regs);
	CLEARF(FLAG_N, regs);
	CLEARF(FLAG_H, regs);
	return 4;
}

static int JR_imm8(opcode_t opcode) {
	regs.PC += BYTE_SIGN_EXT(opcode.low);
	return 12;
}

static int LDI_memHL_A(opcode_t opcode) {
	(void) opcode;
	uint16_t HL = TO_WORD(regs.H, regs.L);
	mem_write8(HL, regs.A);
	HL++;
	SET_PAIR(regs.H, regs.L, HL);
	return 8;
}

static int LDI_A_memHL(opcode_t opcode) {
	(void) opcode;
	uint16_t HL = TO_WORD(regs.H, regs.L);
	regs.A = mem_read8(HL);
	HL++;
	SET_PAIR(regs.H, regs.L, HL);
	return 8;
}

static int LDD_memHL_A(opcode_t opcode) {
	(void) opcode;
	uint16_t HL = TO_WORD(regs.H, regs.L);
	mem_write8(HL, regs.A);
	HL--;
	SET_PAIR(regs.H, regs.L, HL);
	return 8;
}

static int LDD_A_memHL(opcode_t opcode) {
	(void) opcode;
	uint16_t HL = TO_WORD(regs.H, regs.L);
	regs.A = mem_read8(HL);
	HL--;
	SET_PAIR(regs.H, regs.L, HL);
	return 8;
}

/*
 * Reference:
 * https://www.reddit.com/r/EmuDev/comments/4ycoix/a_guide_to_the_gameboys_halfcarry_flag/d6p3rtl/
 */
static int DAA(opcode_t opcode) {
	(void) opcode;

	uint32_t adjustment = 0;
	if (!GETF(FLAG_N, regs)) {
		// addition case: check nibbles and flags
		if (BYTE_LONIBBLE(regs.A) > 0x09 || GETF(FLAG_H, regs))
			adjustment += 0x06;
		if (regs.A > 0x99 || GETF(FLAG_C, regs)) {
			adjustment += 0x60;
			SETF(FLAG_C, regs);
		}
	} else {
		// subtraction case: check flags only
		if (GETF(FLAG_H, regs))
			adjustment += -0x06;
		if (GETF(FLAG_C, regs))
			adjustment += -0x60;
	}
	uint32_t res = regs.A + adjustment;
	regs.A = (uint8_t)(res & 0xff);

	CLEARF(FLAG_H, regs);
	COPY_TOF(FLAG_Z, regs, regs.A == 0);
	return 4;
}

static int CPL(opcode_t opcode) {
	(void) opcode;
	regs.A = ~regs.A;
	SETF(FLAG_H, regs);
	SETF(FLAG_N, regs);
	return 4;
}

static int SCF(opcode_t opcode) {
	(void) opcode;
	SETF(FLAG_C, regs);
	CLEARF(FLAG_H, regs);
	CLEARF(FLAG_N, regs);
	return 4;
}

static int CCF(opcode_t opcode) {
	(void) opcode;
	FLIPF(FLAG_C, regs);
	CLEARF(FLAG_H, regs);
	CLEARF(FLAG_N, regs);
	return 4;
}

static int RET(opcode_t opcode) {
	(void) opcode;
	regs.PC = mem_read16(regs.SP);
	regs.SP += 2;
	return 16;
}

static int CALL_imm16(opcode_t opcode) {
	regs.SP -= 2;
	mem_write16(regs.SP, regs.PC);
	regs.PC = TO_WORD(opcode.high, opcode.low);
	return 24;
}

static int ADD_SP_imm8(opcode_t opcode) {
	uint32_t imm = BYTE_SIGN_EXT(opcode.low);
	uint32_t newSP = regs.SP + imm;

	/*
	 * alternative way
	 * COPY_TOF(FLAG_C, regs, (newSP & 0xff) < (regs.SP & 0xff));
	 * COPY_TOF(FLAG_H, regs, (newSP & 0xf) < (regs.SP & 0xf));
	 */
	compute_carry(BYTE, regs.SP, imm, newSP);
	half_carry(BYTE, regs.SP, imm, newSP);
	regs.SP = newSP & 0xffff;

	CLEARF(FLAG_Z, regs);
	CLEARF(FLAG_N, regs);
	return 16;
}

static int LD_HL_SP_off8(opcode_t opcode) {
	uint32_t imm = BYTE_SIGN_EXT(opcode.low);
	uint32_t newSP = regs.SP + imm;

	compute_carry(BYTE, regs.SP, imm, newSP);
	half_carry(BYTE, regs.SP, imm, newSP);
	SET_PAIR(regs.H, regs.L, newSP & 0xffff);

	CLEARF(FLAG_Z, regs);
	CLEARF(FLAG_N, regs);
	return 12;
}

static int LD_mem8_A(opcode_t opcode) {
	uint16_t addr = 0xff00 | (uint16_t) opcode.low;
	mem_write8(addr, regs.A);
	return 12;
}

static int LD_A_mem8(opcode_t opcode) {
	uint16_t addr = 0xff00 | (uint16_t) opcode.low;
	regs.A = mem_read8(addr);
	return 12;
}

static int LD_memC_A(opcode_t opcode) {
	(void) opcode;
	uint16_t addr = 0xff00 | (uint16_t) regs.C;
	mem_write8(addr, regs.A);
	return 8;
}

static int LD_A_memC(opcode_t opcode) {
	(void) opcode;
	uint16_t addr = 0xff00 | (uint16_t) regs.C;
	regs.A = mem_read8(addr);
	return 8;
}

static int LD_mem16_A(opcode_t opcode) {
	uint16_t addr = TO_WORD(opcode.high, opcode.low);
	mem_write8(addr, regs.A);
	return 16;
}

static int LD_A_mem16(opcode_t opcode) {
	uint16_t addr = TO_WORD(opcode.high, opcode.low);
	regs.A = mem_read8(addr);
	return 16;
}

static int LD_SP_HL(opcode_t opcode) {
	(void) opcode;
	regs.SP = TO_WORD(regs.H, regs.L);
	return 8;
}

static int JP_HL(opcode_t opcode) {
	(void) opcode;
	regs.PC = TO_WORD(regs.H, regs.L);
	return 4;
}

static int JP_imm16(opcode_t opcode) {
	uint16_t addr = TO_WORD(opcode.high, opcode.low);
	regs.PC = addr;
	return 16;
}

static int RET_cond(opcode_t opcode) {
	if (check_condition(GET_CONDITION(opcode.main))) {
		(void) RET(opcode);
		return 20;
	} else {
		return 8;
	}
}

static int JR_cond_imm8(opcode_t opcode) {
	if (check_condition(GET_CONDITION(opcode.main))) {
		(void) JR_imm8(opcode);
		return 12;
	} else {
		return 8;
	}
}

static int JP_cond_imm16(opcode_t opcode) {
	if (check_condition(GET_CONDITION(opcode.main))) {
		(void) JP_imm16(opcode);
		return 16;
	} else {
		return 12;
	}
}

static int CALL_cond_imm16(opcode_t opcode) {
	if (check_condition(GET_CONDITION(opcode.main))) {
		(void) CALL_imm16(opcode);
		return 24;
	} else {
		return 12;
	}
}

static int RST_num(opcode_t opcode) {
	uint16_t addr = GET_RST_NUM(opcode.main) << 3;
	SET_PAIR(opcode.high, opcode.low, addr);
	(void) CALL_imm16(opcode);
	return 16;
}

static int LD_reg8_reg8(opcode_t opcode) {
	enum operand8 src = decode_operand8(GET_REG8_SRC(opcode.main));
	enum operand8 dst = decode_operand8(GET_REG8_DST(opcode.main));
	assert(!(src == MEM_HL && dst == MEM_HL)); // LD (HL), (HL) is HALT
	set_value8(dst, get_value8(src));

	return (dst != MEM_HL && src != MEM_HL) ? 4 : 8;
}

static int LD_reg8_imm8(opcode_t opcode) {
	enum operand8 dst = decode_operand8(GET_REG8_DST(opcode.main));
	set_value8(dst, opcode.low);
	return dst != MEM_HL ? 8 : 12;
}

static int LD_memreg16_A(opcode_t opcode) {
	enum operand16 reg = decode_operand16(GET_REG16(opcode.main));
	mem_write8(get_value16(reg), regs.A);
	return 8;
}

static int LD_A_memreg16(opcode_t opcode) {
	enum operand16 reg = decode_operand16(GET_REG16(opcode.main));
	regs.A = mem_read8(get_value16(reg));
	return 8;
}

static int LD_reg16_imm16(opcode_t opcode) {
	enum operand16 dst = decode_operand16(GET_REG16(opcode.main));
	set_value16(dst, TO_WORD(opcode.high, opcode.low));
	return 12;
}

static int POP_altreg16(opcode_t opcode) {
	enum operand16 dst = decode_operand16_alt(GET_REG16(opcode.main));
	uint16_t val = mem_read16(regs.SP);
	regs.SP += 2;
	set_value16(dst, val);

	return 12;
}

static int PUSH_altreg16(opcode_t opcode) {
	enum operand16 src = decode_operand16_alt(GET_REG16(opcode.main));
	regs.SP -= 2;
	mem_write16(regs.SP, get_value16(src));
	return 16;
}

static int ADD_HL_reg16(opcode_t opcode) {
	enum operand16 src = decode_operand16(GET_REG16(opcode.main));
	uint16_t b = get_value16(src);
	uint16_t HL = TO_WORD(regs.H, regs.L);
	uint32_t res = HL + b;

	compute_carry(WORD, HL, b, res);
	half_carry(WORD, HL, b, res);

	HL = (uint16_t)(res & 0xffff);
	SET_PAIR(regs.H, regs.L, HL);

	CLEARF(FLAG_N, regs);
	return 8;
}

static int INC_reg8(opcode_t opcode) {
	enum operand8 dst = decode_operand8(GET_REG8_DST(opcode.main));
	uint8_t val = get_value8(dst);
	uint32_t res = val + 1;
	set_value8(dst, (uint8_t)(res & 0xff));

	half_carry(BYTE, val, 1, res);
	COPY_TOF(FLAG_Z, regs, !(res & 0xff));
	CLEARF(FLAG_N, regs);
	return dst != MEM_HL ? 4 : 12;
}

static int DEC_reg8(opcode_t opcode) {
	enum operand8 dst = decode_operand8(GET_REG8_DST(opcode.main));
	uint8_t val = get_value8(dst);
	uint32_t res = val + 0xffff;
	set_value8(dst, (uint8_t)(res & 0xff));

	half_borrow(BYTE, val, 1);
	COPY_TOF(FLAG_Z, regs, !(res & 0xff));
	SETF(FLAG_N, regs);
	return dst != MEM_HL ? 4 : 12;
}

static int INC_reg16(opcode_t opcode) {
	enum operand16 dst = decode_operand16(GET_REG16(opcode.main));
	uint16_t val = get_value16(dst);
	set_value16(dst, (uint16_t)((val + 1) & 0xffff));
	return 8;
}

static int DEC_reg16(opcode_t opcode) {
	enum operand16 dst = decode_operand16(GET_REG16(opcode.main));
	uint16_t val = get_value16(dst);
	set_value16(dst, (uint16_t)((val + 0xffff) & 0xffff));
	return 8;
}

enum alu_op {
	ADD = 0x0,
	ADC = 0x1,
	SUB = 0x2,
	SBC = 0x3,
	AND = 0x4,
	XOR = 0x5,
	OR = 0x6,
	CP = 0x7,
};

#define SET_AND_RESTORE(_op, _backup)             \
	do {                                          \
		regs.A = (uint8_t)(res & 0xff);           \
		results[(_op)] = TO_WORD(regs.A, regs.F); \
		SET_PAIR(regs.A, regs.F, (_backup));      \
	} while (false)

static void alu(enum alu_op op, uint8_t value) {
	uint16_t backup = TO_WORD(regs.A, regs.F);
	uint16_t results[8];
	uint32_t res;

	// ADD
	res = regs.A + value;
	compute_carry(BYTE, regs.A, value, res);
	half_carry(BYTE, regs.A, value, res);
	COPY_TOF(FLAG_Z, regs, (res & 0xff) == 0);
	CLEARF(FLAG_N, regs);
	SET_AND_RESTORE(ADD, backup);

	// ADC
	uint8_t carry = GETF(FLAG_C, regs);
	res = regs.A + value + carry;
	half_carry(BYTE, regs.A, value ^ carry, res);
	compute_carry(BYTE, regs.A, value ^ carry, res);
	COPY_TOF(FLAG_Z, regs, (res & 0xff) == 0);
	CLEARF(FLAG_N, regs);
	SET_AND_RESTORE(ADC, backup);

	// SUB
	res = regs.A - value;
	compute_borrow(regs.A, value);
	half_borrow(BYTE, regs.A, value);
	COPY_TOF(FLAG_Z, regs, (res & 0xff) == 0);
	SETF(FLAG_N, regs);
	SET_AND_RESTORE(SUB, backup);

	// SBC
	uint32_t tmp = value + GETF(FLAG_C, regs);
	res = regs.A - tmp;
	half_borrow(BYTE, regs.A, value);
	if (!GETF(FLAG_H, regs))
		half_borrow(BYTE, regs.A - value, GETF(FLAG_C, regs));
	compute_borrow(regs.A, tmp);
	COPY_TOF(FLAG_Z, regs, (res & 0xff) == 0);
	SETF(FLAG_N, regs);
	SET_AND_RESTORE(SBC, backup);

	// AND
	res = regs.A & value;
	CLEARF(FLAG_C, regs);
	SETF(FLAG_H, regs);
	CLEARF(FLAG_N, regs);
	COPY_TOF(FLAG_Z, regs, (res & 0xff) == 0);
	SET_AND_RESTORE(AND, backup);

	// XOR
	res = regs.A ^ value;
	CLEARF(FLAG_C, regs);
	CLEARF(FLAG_H, regs);
	CLEARF(FLAG_N, regs);
	COPY_TOF(FLAG_Z, regs, (res & 0xff) == 0);
	SET_AND_RESTORE(XOR, backup);

	// OR
	res = regs.A | value;
	CLEARF(FLAG_C, regs);
	CLEARF(FLAG_H, regs);
	CLEARF(FLAG_N, regs);
	COPY_TOF(FLAG_Z, regs, (res & 0xff) == 0);
	SET_AND_RESTORE(OR, backup);

	// CP
	res = regs.A - value;
	compute_borrow(regs.A, value);
	half_borrow(BYTE, regs.A, value);
	COPY_TOF(FLAG_Z, regs, (res & 0xff) == 0);
	SETF(FLAG_N, regs);
	res = regs.A;
	SET_AND_RESTORE(CP, backup);

	SET_PAIR(regs.A, regs.F, results[op]);
}
#undef SET_AND_RESTORE

static int ALU_A_imm8(opcode_t opcode) {
	alu(GET_ALU_OP(opcode.main), opcode.low);
	return 8;
}

static int ALU_A_reg8(opcode_t opcode) {
	enum operand8 src = decode_operand8(GET_REG8_SRC(opcode.main));
	alu(GET_ALU_OP(opcode.main), get_value8(src));
	return src != MEM_HL ? 4 : 8;
}

static int EI(opcode_t opcode) {
	(void) opcode;
	IME = true;
	return 4;
}

static int DI(opcode_t opcode) {
	(void) opcode;
	IME = false;
	return 4;
}

static int RETI(opcode_t opcode) {
	(void) opcode;
	IME = true;
	return RET(opcode);
}

static int HALT(opcode_t opcode) {
	(void) opcode;
	CPU_STATE = HALTED;
	return 4;
}

/* TODO: implement me */
static int STOP(opcode_t opcode) {
	(void) opcode;
	CPU_STATE = HALTED;
	return 4;
}

typedef struct op_decode_s {
	uint8_t code;
	uint8_t mask;
	instruction_t fn;
} op_decode_t;

static const op_decode_t decoding_map[] = {
	{ 0x00, 0xff, NOP },
	{ 0x08, 0xff, LD_mem_SP },
	{ 0x07, 0xff, RLC_A },
	{ 0x0f, 0xff, RRC_A },
	{ 0x17, 0xff, RL_A },
	{ 0x1f, 0xff, RR_A },
	{ 0x10, 0xff, STOP },
	{ 0x18, 0xff, JR_imm8 },
	{ 0x22, 0xff, LDI_memHL_A },
	{ 0x2a, 0xff, LDI_A_memHL },
	{ 0x32, 0xff, LDD_memHL_A },
	{ 0x3a, 0xff, LDD_A_memHL },
	{ 0x27, 0xff, DAA },
	{ 0x2f, 0xff, CPL },
	{ 0x37, 0xff, SCF },
	{ 0x3f, 0xff, CCF },
	{ 0x76, 0xff, HALT },
	{ 0xc9, 0xff, RET },
	{ 0xd9, 0xff, RETI },
	{ 0xcd, 0xff, CALL_imm16 },
	{ 0xe8, 0xff, ADD_SP_imm8 },
	{ 0xf8, 0xff, LD_HL_SP_off8 },
	{ 0xe0, 0xff, LD_mem8_A },
	{ 0xf0, 0xff, LD_A_mem8 },
	{ 0xe2, 0xff, LD_memC_A },
	{ 0xf2, 0xff, LD_A_memC },
	{ 0xea, 0xff, LD_mem16_A },
	{ 0xfa, 0xff, LD_A_mem16 },
	{ 0xe9, 0xff, JP_HL },
	{ 0xf9, 0xff, LD_SP_HL },
	{ 0xf3, 0xff, DI },
	{ 0xfb, 0xff, EI },
	{ 0xc3, 0xff, JP_imm16 },
	{ 0xcb, 0xff, NULL }, // cb prefix
	{ 0x02, 0xef, LD_memreg16_A },
	{ 0x0a, 0xef, LD_A_memreg16 },
	{ 0x20, 0xe7, JR_cond_imm8 },
	{ 0xc0, 0xe7, RET_cond },
	{ 0xc2, 0xe7, JP_cond_imm16 },
	{ 0xc4, 0xe7, CALL_cond_imm16 },
	{ 0x01, 0xcf, LD_reg16_imm16 },
	{ 0x09, 0xcf, ADD_HL_reg16 },
	{ 0x03, 0xcf, INC_reg16 },
	{ 0x0b, 0xcf, DEC_reg16 },
	{ 0xc1, 0xcf, POP_altreg16 },
	{ 0xc5, 0xcf, PUSH_altreg16 },
	{ 0x04, 0xc7, INC_reg8 },
	{ 0x05, 0xc7, DEC_reg8 },
	{ 0x06, 0xc7, LD_reg8_imm8 },
	{ 0xc6, 0xc7, ALU_A_imm8 },
	{ 0xc7, 0xc7, RST_num },
	{ 0x40, 0xc0, LD_reg8_reg8 },
	{ 0x80, 0xc0, ALU_A_reg8 },
	{ 0x00, 0x00, illegal_instr }, // UNKNOWN (catch-all)
};
static const size_t decoding_map_len = sizeof(decoding_map) / sizeof(op_decode_t);

static int SWAP(opcode_t opcode) {
	enum operand8 src = decode_operand8(GET_CB_REG8(opcode.low));
	uint8_t value = get_value8(src);
	value = (uint8_t)(((value & 0xf) << 4) | ((value >> 4) & 0xf));
	set_value8(src, value);
	COPY_TOF(FLAG_Z, regs, value == 0);
	CLEARF(FLAG_C, regs);
	CLEARF(FLAG_H, regs);
	CLEARF(FLAG_N, regs);
	return src != MEM_HL ? 8 : 16;
}

static int RRC(opcode_t opcode) {
	enum operand8 src = decode_operand8(GET_CB_REG8(opcode.low));
	uint8_t val = get_value8(src);
	uint8_t lsb = val & 0x01;
	COPY_TOF(FLAG_C, regs, lsb);
	val >>= 1;
	if (lsb)
		val |= 0x80;
	set_value8(src, val);

	COPY_TOF(FLAG_Z, regs, val == 0);
	CLEARF(FLAG_N, regs);
	CLEARF(FLAG_H, regs);
	return src != MEM_HL ? 8 : 16;
}

static int RR(opcode_t opcode) {
	enum operand8 src = decode_operand8(GET_CB_REG8(opcode.low));
	uint8_t val = get_value8(src);
	uint8_t lsb = val & 0x01;
	val >>= 1;
	if (GETF(FLAG_C, regs))
		val |= 0x80;
	COPY_TOF(FLAG_C, regs, lsb);
	set_value8(src, val);

	COPY_TOF(FLAG_Z, regs, val == 0);
	CLEARF(FLAG_N, regs);
	CLEARF(FLAG_H, regs);
	return src != MEM_HL ? 8 : 16;
}

static int RLC(opcode_t opcode) {
	enum operand8 src = decode_operand8(GET_CB_REG8(opcode.low));
	uint8_t val = get_value8(src);
	uint8_t msb = val & 0x80;
	COPY_TOF(FLAG_C, regs, msb);
	val <<= 1;
	if (msb)
		val |= 0x01;
	val &= 0xff;
	set_value8(src, val);

	COPY_TOF(FLAG_Z, regs, val == 0);
	CLEARF(FLAG_N, regs);
	CLEARF(FLAG_H, regs);
	return src != MEM_HL ? 8 : 16;
}

static int RL(opcode_t opcode) {
	enum operand8 src = decode_operand8(GET_CB_REG8(opcode.low));
	uint8_t val = get_value8(src);
	uint8_t msb = val & 0x80;
	val <<= 1;
	if (GETF(FLAG_C, regs))
		val |= 0x01;
	val &= 0xff;
	COPY_TOF(FLAG_C, regs, msb);
	set_value8(src, val);

	COPY_TOF(FLAG_Z, regs, val == 0);
	CLEARF(FLAG_N, regs);
	CLEARF(FLAG_H, regs);
	return src != MEM_HL ? 8 : 16;
}

static int SLA(opcode_t opcode) {
	enum operand8 src = decode_operand8(GET_CB_REG8(opcode.low));
	uint8_t val = get_value8(src);
	uint8_t msb = val & 0x80;
	val <<= 1;
	val &= 0xff;
	COPY_TOF(FLAG_C, regs, msb);
	set_value8(src, val);

	COPY_TOF(FLAG_Z, regs, val == 0);
	CLEARF(FLAG_N, regs);
	CLEARF(FLAG_H, regs);
	return src != MEM_HL ? 8 : 16;
}

static int SRA(opcode_t opcode) {
	enum operand8 src = decode_operand8(GET_CB_REG8(opcode.low));
	uint8_t val = get_value8(src);
	uint8_t msb = val & 0x80;
	uint8_t lsb = val & 0x01;
	val >>= 1;
	val |= msb;
	val &= 0xff;
	COPY_TOF(FLAG_C, regs, lsb);
	set_value8(src, val);

	COPY_TOF(FLAG_Z, regs, val == 0);
	CLEARF(FLAG_N, regs);
	CLEARF(FLAG_H, regs);
	return src != MEM_HL ? 8 : 16;
}

static int SRL(opcode_t opcode) {
	enum operand8 src = decode_operand8(GET_CB_REG8(opcode.low));
	uint8_t val = get_value8(src);
	COPY_TOF(FLAG_C, regs, val & 0x01);
	val >>= 1;
	val &= 0xff;
	set_value8(src, val);

	COPY_TOF(FLAG_Z, regs, val == 0);
	CLEARF(FLAG_N, regs);
	CLEARF(FLAG_H, regs);
	return src != MEM_HL ? 8 : 16;
}

static int BIT(opcode_t opcode) {
	enum operand8 src = decode_operand8(GET_CB_REG8(opcode.low));
	uint8_t shift = decode_operand8(GET_CB_NUM(opcode.low));
	uint8_t val = get_value8(src);

	COPY_TOF(FLAG_Z, regs, !(val & (1 << shift)));
	CLEARF(FLAG_N, regs);
	SETF(FLAG_H, regs);
	return src != MEM_HL ? 8 : 12;
}

static int RES(opcode_t opcode) {
	enum operand8 src = decode_operand8(GET_CB_REG8(opcode.low));
	uint8_t shift = decode_operand8(GET_CB_NUM(opcode.low));
	uint8_t val = get_value8(src);
	uint8_t mask = (uint8_t)((~(1u << shift)) & 0xff);
	set_value8(src, val & mask);
	return src != MEM_HL ? 8 : 16;
}

static int SET(opcode_t opcode) {
	enum operand8 src = decode_operand8(GET_CB_REG8(opcode.low));
	uint8_t shift = decode_operand8(GET_CB_NUM(opcode.low));
	uint8_t val = get_value8(src);
	set_value8(src, val | (1 << shift));
	return src != MEM_HL ? 8 : 16;
}

/* clang-format off */
static const op_decode_t decoding_map_cb[] = {
	{ 0x00, 0xf8, RLC },
	{ 0x08, 0xf8, RRC },
	{ 0x10, 0xf8, RL  },
	{ 0x18, 0xf8, RR  },
	{ 0x20, 0xf8, SLA },
	{ 0x28, 0xf8, SRA },
	{ 0x30, 0xf8, SWAP },
	{ 0x38, 0xf8, SRL },
	{ 0x40, 0xc0, BIT },
	{ 0x80, 0xc0, RES },
	{ 0xc0, 0xc0, SET },
};
/* clang-format on */
static const size_t decoding_map_cb_len = sizeof(decoding_map_cb) / sizeof(op_decode_t);

static const op_decode_t *decode_helper(const op_decode_t *map, size_t len, uint8_t op) {
	for (size_t i = 0; i < len; i++) {
		const op_decode_t *entry = &map[i];
		if ((op & entry->mask) == entry->code)
			return entry;
	}
	return NULL;
}

instruction_t decode(opcode_t opcode) {
	instruction_t fn = decode_helper(decoding_map, decoding_map_len, opcode.main)->fn;
	if (!fn) // cb prefix
		fn = decode_helper(decoding_map_cb, decoding_map_cb_len, opcode.low)->fn;
	return fn;
}
