# seaboy [![pipeline status](https://gitlab.com/TheZxcv/seaboy/badges/master/pipeline.svg)](https://gitlab.com/TheZxcv/seaboy/-/commits/master)[![coverage report](https://gitlab.com/TheZxcv/seaboy/badges/master/coverage.svg)](https://gitlab.com/TheZxcv/seaboy/-/commits/master)

seaboy is a crude _GameBoy_ emulator library made in C.
It includes a companion project that provides a graphical back-end made in C++ using the Qt library.

## Build

```bash
$ mkdir build
$ cd build
$ cmake -G "Unix Makefiles" . ..
$ cmake --build . --target all --config Release
```
## Usage

```bash
$ ./seaboy-qt/seaboy-qt [ROM-file]
```
